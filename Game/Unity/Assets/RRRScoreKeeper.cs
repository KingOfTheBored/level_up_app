using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RRRScoreKeeper : MonoBehaviour
{
    private Text textScore;
    
    // Start is called before the first frame update
    void Start()
    {
        textScore = GetComponent<UnityEngine.UI.Text>();
        textScore.text = FindObjectOfType<RRRGameManager>().score.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        textScore.text = FindObjectOfType<RRRGameManager>().score.ToString();
    }
}
