using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RRRSpawner : MonoBehaviour
{
    public GameObject[] obstacles;
    public GameObject colourChangePrefab;
    private Vector2 lastPosition;
    private Vector2 lastChangePosition;

    // Start is called before the first frame update
    void Start()
    {
        lastChangePosition = new Vector2(0,0);
        for (int i = 0; i < obstacles.Length; i++)
        {
            int index = Random.Range(0,obstacles.Length);
            Instantiate(obstacles[index], new Vector2(0, 4+(i*8)), Quaternion.identity);
            int r = Random.Range(0,2);
            lastChangePosition.y+=8+4*r;
            Instantiate(colourChangePrefab, lastChangePosition, Quaternion.identity);
        }    
        lastPosition = new Vector2(0, 4+((obstacles.Length-1)*8));
    }

    public void AddNewObstacle(){
        lastPosition.y+=8;
        int index = Random.Range(0,obstacles.Length);
        Instantiate(obstacles[index], lastPosition, Quaternion.identity);
    }

    public void AddNewColourChange(){
        int index = Random.Range(0,2);
        lastChangePosition.y+=8+4*index;
        Instantiate(colourChangePrefab, lastChangePosition, Quaternion.identity);
    }
}
