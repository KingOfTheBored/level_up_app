using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BBBGameManager : GameManager
{
    public float slowness = 10f;
    private float nextIncrement;
    private bool endGame = false;

    public void Start(){
        base.StartTimer();
        nextIncrement = Time.time;
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }
    // Start is called before the first frame update
    public void EndGame()
    {
        StartCoroutine(RestartLevel());
    }

    IEnumerator RestartLevel(){
        endGame = true;
        Time.timeScale = 1f / slowness;
        Time.fixedDeltaTime = Time.fixedDeltaTime / slowness;

        yield return new WaitForSeconds(1f/slowness);

        Time.timeScale = 1f;
        Time.fixedDeltaTime = Time.fixedDeltaTime * slowness;
        endGame = false;
        SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex);
    }

    public void AddScore(){
        if(Time.time>nextIncrement && !endGame){
            base.AddScore(2);
            nextIncrement=Time.time +.1f;        
        }
    }
}
