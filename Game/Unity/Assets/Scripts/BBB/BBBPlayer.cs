using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BBBPlayer : MonoBehaviour
{
    public float speed = 15f;
    public float mapWidth = 5f;
    public float movement = 0f;
    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update(){
        if(Input.GetMouseButton(0)){
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if(mousePos.x < 0){
                movement = -1f;
            }else{
                movement = 1f;
            }
        }else{
            movement = 0f;
        }
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float x = movement * Time.fixedDeltaTime * speed;

        Vector2 newPosition = rb.position + Vector2.right * x;
        newPosition.x = Mathf.Clamp(newPosition.x, -mapWidth, mapWidth);
        rb.MovePosition(newPosition);

    }

    void OnCollisionEnter2D(){
        FindObjectOfType<BBBGameManager>().EndGame();
    }
}
