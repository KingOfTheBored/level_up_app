using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BBBScoreKeeper : MonoBehaviour
{
    private Text textScore;
    
    // Start is called before the first frame update
    void Start()
    {
        textScore = GetComponent<UnityEngine.UI.Text>();
        textScore.text = FindObjectOfType<BBBGameManager>().score.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        textScore.text = FindObjectOfType<BBBGameManager>().score.ToString();
    }
}
