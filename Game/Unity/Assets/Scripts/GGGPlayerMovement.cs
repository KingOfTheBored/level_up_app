using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GGGPlayerMovement : MonoBehaviour
{
    public float movementSpeed = 600.0f;
    float movement = 0.0f;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButton(0)){
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if(mousePos.x < 0){
                movement = -1f;
            }else{
                movement = 1f;
            }
        }else{
            movement = 0f;
        }
        
        // movement = Input.GetAxisRaw("Horizontal");
    }

    private void FixedUpdate(){
        transform.RotateAround(Vector3.zero, Vector3.forward, movement * Time.fixedDeltaTime * -movementSpeed);
    }

    private void OnTriggerEnter2D(Collider2D collision){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
