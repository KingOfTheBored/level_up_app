using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public float timeRemaining;
    public int score;

    void Awake ()   
       {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy (gameObject);
        }
    }
    
    // Start is called before the first frame update
    public void StartTimer()
    {
        score = 0;
        timeRemaining = 60f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
        if (timeRemaining > 0)
        {
            timeRemaining -= Time.unscaledDeltaTime;
        }
        else
        {
            finishGame();
        }
    }

    void finishGame(){
        try
        {
            AndroidJavaClass jc = new AndroidJavaClass("com.example.levelup.game.OverrideUnityActivity");
            AndroidJavaObject overrideActivity = jc.GetStatic<AndroidJavaObject>("instance");
            overrideActivity.Call("showSendDataActivity", score);
            Application.Quit();
        } catch(Exception e){ }
    }

    public void AddScore(int val){
        score+=val;
    }

    public void QuitGame(){
        Application.Quit();
    }
}
