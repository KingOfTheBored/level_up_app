using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float movementSpeed;
    public Rigidbody2D rb;
    public Joystick joystick;
    public float sensitivity = .2f;

    float horizontalMovement;
    float verticalMovement;
    Vector2 movement;

    // Update is called once per frame
    void Update()
    {
        if (joystick.Horizontal >= sensitivity){
            horizontalMovement = movementSpeed;
        } else if (joystick.Horizontal <= -sensitivity){
            horizontalMovement = -movementSpeed;
        } else {
            horizontalMovement = 0;
        }

        if (joystick.Vertical >= sensitivity){
            verticalMovement = movementSpeed;
        } else if (joystick.Vertical  <= -sensitivity){
            verticalMovement = -movementSpeed;
        } else {
            verticalMovement = 0;
        }

        movement.x = horizontalMovement;
        movement.y = verticalMovement;
    }

    void FixedUpdate(){
        rb.MovePosition(rb.position + movement);
    }
}
