using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RRRGameManager : GameManager
{
    // Start is called before the first frame update
    void Start()
    {
        base.StartTimer();
        score = 0;
        Screen.orientation = ScreenOrientation.Portrait;

    }

    //     // Adds GUI onto the screen
    // void OnGUI()
    // {
    //     GUIStyle style = new GUIStyle("button");
    //     style.fontSize = 35;        
    //     if (GUI.Button(new Rect(20, 1850, 200, 100), "Quit Game", style)) Application.Quit();
    // }

    public void ResetGame(){
        SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex);
    }

}
