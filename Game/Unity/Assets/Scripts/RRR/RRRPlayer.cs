using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RRRPlayer : MonoBehaviour
{
    public float jumpForce = 8f;
    public Rigidbody2D rb;
    public string currentColour;
    public SpriteRenderer sr;

    private bool gravity = false;

    public Color colourPurple;
    public Color colourOrange;
    public Color colourBlue;
    public Color colourYellow;

    void Start(){
        GetComponent<Rigidbody2D>().gravityScale = 0;
        SetRandomColour();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0))
        {
            if (!gravity){
                gravity = true;
                GetComponent<Rigidbody2D>().gravityScale = 3;
            }
            rb.velocity = Vector2.up * jumpForce;
        }
    }

    void FixedUpdate(){
        if(Camera.main.WorldToScreenPoint(transform.position).y<-500){
            FindObjectOfType<RRRGameManager>().ResetGame();
        }
    }

    void SetRandomColour(){
        string pastColour = currentColour;
        while (pastColour == currentColour)
        {
            int index = Random.Range(0,4);
            switch (index)
            {
                case 0: 
                    currentColour = "Purple";
                    sr.color = colourPurple;
                    break;
                case 1: 
                    currentColour = "Orange";
                    sr.color = colourOrange;
                    break;
                case 2: 
                    currentColour = "Yellow";
                    sr.color = colourYellow;
                    break;
                case 3: 
                    currentColour = "Blue";
                    sr.color = colourBlue;
                    break;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col){
        if (col.tag == "Point"){
            FindObjectOfType<RRRGameManager>().AddScore(6);
            Destroy(col);
        } else if (col.tag == "Colour Change"){
            SetRandomColour();
            FindObjectOfType<RRRSpawner>().AddNewColourChange();
            Destroy(col.gameObject);
        } else if (col.tag != currentColour){
            FindObjectOfType<RRRGameManager>().ResetGame();
        }
    }
}
