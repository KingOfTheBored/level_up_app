using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float rotationSpeed = 100f;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0f, 0f, rotationSpeed * Time.deltaTime); 
        if(Camera.main.WorldToScreenPoint(transform.position).y<-500){
            FindObjectOfType<RRRSpawner>().AddNewObstacle();
            Destroy(gameObject);
        }
    }
}

