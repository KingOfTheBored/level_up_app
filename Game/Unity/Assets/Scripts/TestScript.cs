using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TestScript : MonoBehaviour
{
    public Text txt;
    // string to be passed back to Android
    string lastStringColor = "";

    // Start is called before the first frame update
    void Start()
    {        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
    }

    void ChangeColor(string newColor)
    {
        lastStringColor = newColor;
        addMessage(newColor);

        if (newColor == "red") txt.color = Color.red;
        else if (newColor == "blue") txt.color = Color.blue;
        // else if (newColor == "yellow") txt.color = Color.yellow;
        else txt.color = Color.black;
    }

    public void CallFromAndroid(string msgFromAndroid) {
        txt.text = msgFromAndroid;
    }

    void showHostMainWindow()
    {
        try
        {
            AndroidJavaClass jc = new AndroidJavaClass("com.example.levelup.game.OverrideUnityActivity");
            AndroidJavaObject overrideActivity = jc.GetStatic<AndroidJavaObject>("instance");
            overrideActivity.Call("showMainActivity", lastStringColor);
        } catch(Exception e)
        {
        }
    }

    void finishGame(){
        try
        {
            AndroidJavaClass jc = new AndroidJavaClass("com.example.levelup.game.OverrideUnityActivity");
            AndroidJavaObject overrideActivity = jc.GetStatic<AndroidJavaObject>("instance");
            overrideActivity.Call("showSendDataActivity");
        } catch(Exception e){ }
    }
    
    void addMessage(string color)
    {
        try
        {
            AndroidJavaClass jc = new AndroidJavaClass("com.example.levelup.game.OverrideUnityActivity");
            AndroidJavaObject overrideActivity = jc.GetStatic<AndroidJavaObject>("instance");
            overrideActivity.Call("addMessage", color);
        } catch(Exception e)
        {
        }
    }
}
