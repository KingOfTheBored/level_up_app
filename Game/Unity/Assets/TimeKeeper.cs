using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeKeeper : MonoBehaviour
{
    private Text textTime;
    
    // Start is called before the first frame update
    void Start()
    {
        textTime = GetComponent<UnityEngine.UI.Text>();
        textTime.text = (FindObjectOfType<GameManager>().timeRemaining).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        textTime.text = (FindObjectOfType<GameManager>().timeRemaining).ToString();
    }
}
