# Habit Devleopment App Project

## Coding Conventions
Coding conventions used from the following
https://github.com/ribot/android-guidelines/blob/master/project_and_code_guidelines.md

## Intergrating Unity game
### Unity
In Unity, export an android build to a folder (Remember the location of this folder as you will need to add it as a project directory in android studio).

### Android Studio
Add the following lines to your projects **settings.gradle** file, using the directory you exported your unity build:
```
include ':unityLibrary'
project(':unityLibrary').projectDir=new File('exported_unity_gamedirectory)\\unityLibrary')
```

In **gradle.properties** add the following line:
```
unityStreamingAssets=.unity3d
```

In **build.gradle:Project** add the following line under allprojects/repositories:
```
flatDir {
            dirs "${project(':unityLibrary').projectDir}/libs"
        }
```

In **build.gradle:Module** add the following line under defaultconfig:
```
multiDexEnabled true
```

And under dependencies:
```
    implementation project(':unityLibrary')
    implementation fileTree(dir: project(':unityLibrary').getProjectDir().toString() + ('\\libs'), include: ['*.jar'])
```

In **string.xml** add the line:
```
 <string name="game_view_content_description">Game view</string>
```

In the **manifest.xml** when adding the activity (UnityActivity) which will contain the unity game add the following lines:
```
<activity
    android:name=".UnityActivity"
    android:screenOrientation="fullSensor"
    android:configChanges="mcc|mnc|locale|touchscreen|keyboard|keyboardHidden|navigation|orientation|screenLayout|uiMode|screenSize|smallestScreenSize|fontScale|layoutDirection|density"
    android:hardwareAccelerated="false"
    android:process=":Unity"
    android:label="@string/app_name">
</activity>
```

### Remove additional icon
adding a unity game will cause a second icon to be created when you load the app onto a phone. To remove this, delete the intent filter tag from the unity library manifest file. Note: you should uninstall the app from your phone first or it will delete both icons from it.

## Launching firebase functions
Firebase functions are created under the functions folder, in the index.js file.
Use the following line in node.js to deploy the fire functions:
```
firebase deploy --only functions
```

## Game Picker
1. Sequentially run the code in the Machine Learning notebook
2. Move the files generated in that notebook to the directory
3. In that same directory you can choose which model is used in the app in the MachineLearning.py file with the **reloadmodel()** method

## Creating a Emotion Detection model
This model is created with the use of several Juypter notebooks in the NLP/ directory, steps on how each one works are described in the notebooks but here is an outline:
1. **Collect Tweets.ipynb**: is used to collect tweets using specific hashtags and emojis, these have to be listed in the query_relations.json file with it's matched emotion
2. **DatasetCreator.ipynb**: is used to create a dataset from the collected tweets.
3. **EmotionDatasetValidation.ipynb**: is used to validate the dataset to check that it is balanced.
4. **EmotionModel.ipynb**: is used to create the neural network model using the dataset that was created.
5. **EmotionModelValidation.ipynb**: is used to validate the model created using validation dataset.
###  Loading the model on android
In the directory **app\src\main\python** save the weights file saved in the **EmotionModel.ipynb** as **emotion_recognition_model_weights.h5**. The parameters used in the **Emotion Detector.py** file must match the ones used when creating the model from **EmotionModel.ipynb**.
