package com.example.levelup

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.room.Room
import com.example.levelup.databinding.ActivityCreateHabitBinding
import com.example.levelup.habitManager.*
import com.example.levelup.helper.SetTime
import com.example.levelup.notifications.Notification


class CreateHabitActivity : AppCompatActivity() {
    private lateinit var mSetTime: SetTime
    private lateinit var mDb: HabitLibrary
    private val mHabitViewModel: HabitViewModel by viewModels {
        HabitViewModelFactory((application as HabitsApplication).repository)
    }
    private var newHabit:Habit = Habit("",
        HabitType.POSITIVE,
        DifficultyLevel.TRIVIAL,
        HabitDuration.FIVE_MIN)
    private var _binding: ActivityCreateHabitBinding? = null
    private val binding get() = _binding!!
    private val mDurationListener: DurationListAdapter = DurationListAdapter()
    private val mDifficultyListener: DifficultyListAdapter = DifficultyListAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityCreateHabitBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Instantiate database
        mDb = Room
            .databaseBuilder(applicationContext, HabitLibrary::class.java, "habit_library")
            .allowMainThreadQueries()
            .build()

        // Disables button till input is valid
        binding.btnSubmitHabit.isEnabled = false

        binding.btnSubmitHabit.setOnClickListener {
            createNewHabit()
            finish()
        }

        binding.inputHabitName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                binding.btnSubmitHabit.isEnabled = !s.isNullOrBlank() && s.isNotEmpty()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.switchType.isChecked = true
        binding.switchType.setOnCheckedChangeListener { _, on ->
            if (on){
                newHabit.type = HabitType.POSITIVE
            }else{
                newHabit.type = HabitType.NEGATIVE
            }
        }

        binding.spinnerDuration.adapter = ArrayAdapter<HabitDuration>(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            HabitDuration.values()
        )
        binding.spinnerDuration.onItemSelectedListener = mDurationListener

        binding.spinnerDifficulty.adapter = ArrayAdapter<DifficultyLevel>(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            DifficultyLevel.values()
        )
        binding.spinnerDifficulty.onItemSelectedListener = mDifficultyListener
        binding.spinnerDifficulty.setSelection(0)

        binding.txtSetReminder.visibility = View.GONE
        binding.inTextReminderCreate.visibility = View.GONE
        binding.switchReminder.setOnCheckedChangeListener{ _, on ->
            if (on){
                binding.txtSetReminder.visibility = View.VISIBLE
                binding.inTextReminderCreate.visibility = View.VISIBLE
            }else{
                binding.txtSetReminder.visibility = View.GONE
                binding.inTextReminderCreate.visibility = View.GONE
            }
        }

        mSetTime = SetTime(binding.inTextReminderCreate, this)
        mSetTime.pickedHour = 12
        mSetTime.pickedMinute = 0
        binding.inTextReminderCreate.setText(String.format("%02d", 12)+" : "+String.format("%02d", 0))

        binding.txtCueDescription.visibility = View.GONE
        binding.inTxtCueLayout.visibility = View.GONE
        binding.switchCue.setOnCheckedChangeListener{_, on ->
            if (on){
                binding.txtCueDescription.visibility = View.VISIBLE
                binding.inTxtCueLayout.visibility = View.VISIBLE
            }else{
                binding.txtCueDescription.visibility = View.GONE
                binding.inTxtCueLayout.visibility = View.GONE
            }
        }
        binding.switchCue.isChecked = false

        binding.txtMotivationDescription.visibility = View.GONE
        binding.inTxtMotivation.visibility = View.GONE
        binding.switchMotivation.setOnCheckedChangeListener{_, on ->
            if (on){
                binding.txtMotivationDescription.visibility = View.VISIBLE
                binding.inTxtMotivation.visibility = View.VISIBLE
            }else{
                binding.txtMotivationDescription.visibility = View.GONE
                binding.inTxtMotivation.visibility = View.GONE
            }
        }
        binding.switchMotivation.isChecked = false
    }

    private fun createNewHabit(){
        val name = binding.inputHabitName.text.toString()
        val type = if (binding.switchType.isChecked) HabitType.POSITIVE else HabitType.NEGATIVE
        val duration = mDurationListener.duration
        val difficulty = mDifficultyListener.difficulty
        val reminder = if (binding.switchReminder.isChecked) binding.inTextReminderCreate.text.toString() else ""
        val cue = if (binding.switchCue.isChecked) binding.inTxtCue.text.toString() else ""
        val motivation = if (binding.switchMotivation.isChecked) binding.inTxtMotivation.text.toString() else ""
        val reminderId = if (reminder == "") -1 else 1

        mHabitViewModel.insert(Habit(name, type, difficulty, duration, cue, motivation, reminder, reminderId))
        mHabitViewModel.allHabits.observe(this, Observer { habitsList ->
            val habit = habitsList.last()
            if (reminderId==1){
                Notification.addReminder(this, mSetTime, habit.habitId, "Reminder to do habit : $name")
            }
        })
    }

    private class DurationListAdapter: AdapterView.OnItemSelectedListener {
        private var _duration = HabitDuration.FIVE_MIN
        val duration get() = _duration

        override fun onNothingSelected(p0: AdapterView<*>?) {
            TODO("Not yet implemented")
        }

        override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
            _duration = parent.getItemAtPosition(pos) as HabitDuration
        }
    }

    private class DifficultyListAdapter: AdapterView.OnItemSelectedListener {
        private var _difficulty = DifficultyLevel.TRIVIAL
        val difficulty get() = _difficulty

        override fun onNothingSelected(p0: AdapterView<*>?) {
            TODO("Not yet implemented")
        }

        override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
            _difficulty = parent.getItemAtPosition(pos) as DifficultyLevel
        }
    }
}