package com.example.levelup

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.bumptech.glide.Glide
import com.chaquo.python.Python
import com.chaquo.python.android.AndroidPlatform
import com.example.levelup.habitManager.HabitLibrary
import com.example.levelup.helper.Helper
import com.example.levelup.settings.SettingsActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.material.navigation.NavigationView
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class DashBoardActivity : AppCompatActivity() {
    private lateinit var mAppBarConfiguration: AppBarConfiguration
    private lateinit var mDb: HabitLibrary
    private val CHANNEL_ID = "Habit_Channel"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dash_board)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        createNotificationChannel()

        if (! Python.isStarted()) {
            Python.start(AndroidPlatform(this@DashBoardActivity))
        }

        createModels()

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_game
                , R.id.nav_slideshow, R.id.nav_help
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, mAppBarConfiguration)
        navView.setupWithNavController(navController)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.dash_board, menu)
        val account: GoogleSignInAccount? = GoogleSignIn.getLastSignedInAccount(applicationContext)
        if(account != null){

            val personsName: String? = account.displayName
            val personsEmail: String? = account.email
            val personsPic: String? = account.photoUrl.toString()
            val username: TextView = findViewById(R.id.username)
            val email: TextView = findViewById(R.id.txtWelcomeMsg)
            val profilePic: ImageView = findViewById(R.id.imageView)

            username.text = personsName
            email.text = personsEmail

            Glide.with(this).load(personsPic).into(profilePic)
        }

        refreshTickets()

        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(mAppBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.action_sign_out -> {
                signOut()
                true
            }
            R.id.action_settings -> {
                val settingsActivityIntent = Intent(this, SettingsActivity::class.java)
                startActivity(settingsActivityIntent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun signOut(){
        val launchMainActivityIntent = Intent(this, MainActivity::class.java)
        launchMainActivityIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_SINGLE_TOP
        launchMainActivityIntent.putExtra("signOut", true)
        startActivity(launchMainActivityIntent)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
                vibrationPattern = longArrayOf(0,300,100,300,100,600)
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun createModels(){
        GlobalScope.launch { // launch a new coroutine in background and continue
            val py = Python.getInstance()
            py.getModule("Emotion Detector")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun refreshTickets(){
        // Get current user's day
        val currentDay = Helper.getCurrentUserDayNo().toInt()
        val recordedDay = Helper.readUserSettings(this, getString(R.string.recorded_day), "0").toInt()
        if (currentDay != recordedDay){
            // reset tickets to 0 and set recorded day to current day
            Helper.writeUserSettings(this, getString(R.string.ticket), "0")
            Helper.writeUserSettings(this, getString(R.string.recorded_day), currentDay.toString())
        }
    }
}