package com.example.levelup

import androidx.lifecycle.*
import com.example.levelup.habitManager.Day
import com.example.levelup.habitManager.DayRepository
import kotlinx.coroutines.launch

class DayViewModel (private val repository: DayRepository): ViewModel()  {
    fun insert(day: Day) = viewModelScope.launch {
        repository.insert(day)
    }

    fun update(day: Day) = viewModelScope.launch {
        repository.update(day)
    }

    fun delete(day: Day) = viewModelScope.launch  {
        repository.delete(day)
    }

    fun habitLastDay(habitId: Int): LiveData<Day> {
        return repository.habitLastDay(habitId).asLiveData()
    }

    fun daysByHabit(habitId:Int): LiveData<List<Day>> {
        return repository.allHabitDays(habitId).asLiveData()
    }

    fun habitDay(habitId: Int, dayNo: Int): LiveData<Day> {
        return repository.habitDay(habitId, dayNo).asLiveData()
    }
}

class DayViewModelFactory(private val repository: DayRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DayViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return DayViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}