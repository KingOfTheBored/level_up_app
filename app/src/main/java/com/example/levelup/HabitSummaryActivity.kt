package com.example.levelup

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.GridView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import com.example.levelup.adapter.DayGridAdapter
import com.example.levelup.databinding.ActivityHabitSummaryBinding
import com.example.levelup.habitManager.Day
import com.example.levelup.habitManager.Habit
import com.example.levelup.helper.Helper
import com.example.levelup.helper.SetTime
import com.example.levelup.notifications.Notification
import java.util.*
import java.util.concurrent.TimeUnit

class HabitSummaryActivity : AppCompatActivity() {
    private val habitViewModel: HabitViewModel by viewModels {
        HabitViewModelFactory((this.application as HabitsApplication).repository)
    }
    private val dayViewModel: DayViewModel by viewModels {
        DayViewModelFactory((this.application as HabitsApplication).dayRepository)
    }

    private var _binding: ActivityHabitSummaryBinding? = null
    private val binding get() = _binding!!

    private lateinit var mHabit: Habit
    private var mDayNo: Int = 0
    private lateinit var mCurrentDay: Day
    private lateinit var mHabitObserver: LiveData<Habit>
    private lateinit var mDaysObserver: LiveData<Day>
    private var mFirstTime = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityHabitSummaryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val habitId = intent.getIntExtra("HabitId",-1)

//        dayViewModel.habitDay(habitId,9).observe(this, androidx.lifecycle.Observer{ day ->
//                dayViewModel.delete(day)
//        })

        mHabitObserver = habitViewModel.habitById(habitId)
        mHabitObserver.observe(this, androidx.lifecycle.Observer{ habit ->
            mHabit = habit
            mDayNo = TimeUnit.MILLISECONDS
                .toDays(mHabit.calculateAge())
                .toInt()

            binding.txtHabitName.text = mHabit.name
            binding.txtHabitDuration.text = mHabit.duration.toString()
            binding.txtHabitDifficulty.text = mHabit.difficulty.toString()

            if(mHabit.cue != ""){
                binding.titleCue.visibility = View.VISIBLE
                binding.txtHabitCue.visibility = View.VISIBLE
                binding.txtHabitCue.text = mHabit.cue
            }else{
                binding.titleCue.visibility = View.GONE
                binding.txtHabitCue.visibility = View.GONE
            }

            if(mHabit.motivation != ""){
                binding.titleMotivation.visibility = View.VISIBLE
                binding.txtMotivationDescription.visibility = View.VISIBLE
                binding.txtMotivationDescription.text = mHabit.motivation
            }else{
                binding.titleMotivation.visibility = View.GONE
                binding.txtMotivationDescription.visibility = View.GONE
            }

            binding.txtHabitAge.text = getString(R.string.days, mDayNo)

            if(habit.reminderId == 1){
                binding.switchReminderSummary.isChecked = true
                binding.txtSetTime.visibility = View.VISIBLE
                binding.inTxtReminder.visibility = View.VISIBLE
                binding.inTxtReminder.setText(mHabit.reminder)
            }else{
                binding.switchReminderSummary.isChecked = false
                binding.txtSetTime.visibility = View.GONE
                binding.inTxtReminder.visibility = View.GONE
            }

            updateGrid(habitId)
        })

        binding.switchReminderSummary.setOnCheckedChangeListener{_, on ->
            if (on){
                mHabit.reminderId = 1
                binding.inTxtReminder.text.toString().let{ time ->
                    if (time != "") {
                        retoggleReminder(time, habitId)
                    }
                }
            }else{
                mHabit.reminderId = -1
                val msg =
                    if (mHabit.cue == "") "Reminder to do habit : ${mHabit.name}"
                    else "Have you done your cue? : ${mHabit.cue}"
                Notification.cancelNotification(this, habitId, msg)
            }
            habitViewModel.update(mHabit)
        }

        binding.btnDoHabit.setOnClickListener{ onActionClick() }

        val setTime = SetTime(binding.inTxtReminder, this)
        binding.inTxtReminder.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if(setTime.set){
                    setTime.set = false
                    val msg =
                        if (mHabit.cue == "") "Reminder to do habit : ${mHabit.name}"
                        else "Have you done your cue? : ${mHabit.cue}"
                    addReminder(setTime, habitId, msg)
                    mHabit.reminder = s.toString()
                    habitViewModel.update(mHabit)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.habit_summary, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.action_delete -> {
                deleteHabit()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun onActionClick(){
        mCurrentDay.noOfActions += 1
        dayViewModel.update(mCurrentDay)

        // increment ticket
        var ticket = Helper.readUserSettings(this, getString(R.string.ticket), "0").toInt()
        ticket++
        Helper.writeUserSettings(this, getString(R.string.ticket), ticket.toString())
    }

    private fun updateGrid(habitId: Int) {
        // gets current day
        mDaysObserver = dayViewModel.habitDay(habitId, mDayNo)
            mDaysObserver.observe(this,
            androidx.lifecycle.Observer { day ->
                // if first time activity is visited today
                if (day == null) {
                    if (mFirstTime) {
                        mFirstTime = false
                        mCurrentDay = Day(habitId, mDayNo, 0)
                        val liveLastDay = dayViewModel.habitLastDay(habitId)
                        liveLastDay.observe(this, androidx.lifecycle.Observer { last ->
                            val lastDay = last?.dayNumber ?: -1
                            fillMissedDay(lastDay)
                            dayViewModel.insert(mCurrentDay)
                            Log.i("habit summary", "current day added")
                            // only need to observe this once
                            liveLastDay.removeObservers(this)
                        })
                    }
                } else {
                    mCurrentDay = day
                    val practiseNo = day.noOfActions
                    binding.txtAction.text = getString(R.string.habit_practise_sentence, practiseNo)
                }
            }
        )

        // fills grid with rest of week
        dayViewModel.daysByHabit(habitId).observe(this,
            androidx.lifecycle.Observer { days ->
                // Update the cached copy of the words in the adapter.
                val fullGrid = completeWeek(days)
                val gridView = findViewById<View>(R.id.gridDays) as GridView
                val dayGridAdapter = DayGridAdapter(
                    fullGrid,
                    mDayNo,
                    this
                )
                gridView.adapter = dayGridAdapter
                gridView.smoothScrollToPosition(mDayNo)
                binding.txtCurrentStreak.text = Day.currentStreak(days, mDayNo).toString()
                binding.txtLongestStreak.text = Day.longestStreak(days).toString()
            }
        )
    }

    private fun fillMissedDay(lastDay: Int){
        for (i in (lastDay+1 until mDayNo)){
            // sets days that have been missed number of actions to 0
            dayViewModel.insert(Day(mHabit.habitId, i, 0))
        }
    }

    private fun completeWeek(list: List<Day>): List<Day>{
        val limit = ((mDayNo+7)/7)*7
        val extra = limit - (mDayNo+1)
        val newList = list.toMutableList()

        for(i in (1..extra)){
            newList.add(Day(0,mDayNo+i, 0))
        }

        return newList
    }

    private fun addReminder(setTime:SetTime, id: Int, msg: String){
        val cal = Calendar.getInstance()
        val currentHour = cal.get(Calendar.HOUR_OF_DAY)
        val currentMinute = cal.get(Calendar.MINUTE)

        val hour = setTime.pickedHour
        val minute = setTime.pickedMinute
        if(currentHour>hour || (currentHour==hour && currentMinute>=minute)){
            cal.add(Calendar.DATE, 1)
        }

        cal.set(Calendar.HOUR_OF_DAY, hour)
        cal.set(Calendar.MINUTE, minute)

        /*val pattern = "yyyy-MM-dd (hh:mm)"
        val simpleDateFormat = SimpleDateFormat(pattern)
        val date: String = simpleDateFormat.format(cal.timeInMillis)*/

        val left = timeLeft(cal.timeInMillis - System.currentTimeMillis())

        Toast.makeText(this, "$left till next reminder", Toast.LENGTH_LONG)
            .show()
        Notification.tapNotification(this, cal.timeInMillis, id, msg)
    }

    private fun retoggleReminder(timeText: String, habitId: Int){
        val textTimes = timeText.split(" : ")
        val hour = textTimes[0].toInt()
        val minute = textTimes[1].toInt()
        val setTime = SetTime(binding.inTxtReminder, this)
        setTime.pickedHour = hour
        setTime.pickedMinute = minute
        val msg =
            if (mHabit.cue == "") "Reminder to do habit : ${mHabit.name}"
            else "Have you done your cue? : ${mHabit.cue}"
        addReminder(setTime, habitId, msg)
    }

    private fun timeLeft(millSec: Long): String{
        val seconds = millSec / 1000 % 60
        val minutes = millSec / (1000 * 60) % 60
        val hours = millSec / (1000 * 60 * 60)
        val hourString = if (hours>0) "$hours hours," else ""
        val minuteString = if (minutes>0) "$minutes minutes," else ""
        val secondString = if (seconds>0) "$seconds, seconds" else ""
        return "$hourString $minuteString $secondString"
    }

    private fun deleteHabit(){
        Notification.cancelNotification(this, mHabit.habitId, "Reminder to do habit : ${mHabit.name}")
        mHabitObserver.removeObservers(this)
        mDaysObserver.removeObservers(this)
        habitViewModel.delete(mHabit)
        finish()
    }
}