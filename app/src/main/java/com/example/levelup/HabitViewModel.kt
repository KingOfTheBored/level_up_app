package com.example.levelup

import androidx.lifecycle.*
import com.example.levelup.habitManager.Habit
import com.example.levelup.habitManager.HabitRepository
import kotlinx.coroutines.launch

class HabitViewModel(private val repository: HabitRepository): ViewModel() {
    // Using LiveData and caching what allWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val allHabits: LiveData<List<Habit>> = repository.allHabits.asLiveData()

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(word: Habit) = viewModelScope.launch {
        repository.insert(word)
    }

    fun update(word: Habit) = viewModelScope.launch {
        repository.update(word)
    }

    fun delete(word: Habit) = viewModelScope.launch {
        repository.delete(word)
    }

    fun habitById(id:Int): LiveData<Habit> {
        return repository.habitById(id).asLiveData()
    }
}

class HabitViewModelFactory(private val repository: HabitRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HabitViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return HabitViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}