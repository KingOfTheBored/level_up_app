package com.example.levelup

import android.app.Application
import com.example.levelup.habitManager.DayRepository
import com.example.levelup.habitManager.HabitLibrary
import com.example.levelup.habitManager.HabitRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class HabitsApplication : Application() {
    // No need to cancel this scope as it'll be torn down with the process
    private val applicationScope = CoroutineScope(SupervisorJob())

    // Using by lazy so the database and the repository are only created when they're needed
    // rather than when the application starts
    private val database by lazy { HabitLibrary.getDatabase(this, applicationScope) }
    val repository by lazy { HabitRepository(database.habitDoa()) }
    val dayRepository by lazy { DayRepository(database.dayDoa()) }
}