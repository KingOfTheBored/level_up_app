package com.example.levelup

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth

class LauncherActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(FirebaseAuth.getInstance().currentUser!=null){
            val intent = Intent(this@LauncherActivity, DashBoardActivity::class.java)
            startActivity(intent)
            finish()
        }else{
            val intent = Intent(this@LauncherActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}