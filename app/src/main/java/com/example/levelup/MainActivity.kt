package com.example.levelup

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider

class MainActivity : AppCompatActivity() {
    private lateinit var mSignInButton: SignInButton
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var mAuth: FirebaseAuth
    private var RC_SIGN_IN = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mSignInButton = findViewById(R.id.signInButton)
        mAuth = FirebaseAuth.getInstance()

        val gso:GoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        handleIntent(intent)

        mSignInButton.setOnClickListener{
                signIn()
        }
    }

    private fun signIn() {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun signOut(){
        mAuth.signOut()
        mGoogleSignInClient.signOut()
        Toast.makeText(this@MainActivity, "You Have Logged Out", Toast.LENGTH_SHORT).show()
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN){
            if (resultCode == Activity.RESULT_OK) {
                val task: Task<GoogleSignInAccount> =
                    GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(task)
            }
        }
    }

    private fun handleIntent(intent: Intent?) {
        if (intent == null || intent.extras == null){
            val account: GoogleSignInAccount? = GoogleSignIn.getLastSignedInAccount(applicationContext)
            if(account != null){
                signIn()
            }
            return
        }
        if(intent.extras!!.containsKey("signOut")) signOut()
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val acc: GoogleSignInAccount? = completedTask.getResult(ApiException::class.java)
            Toast.makeText(this@MainActivity, "Signed In Successfully", Toast.LENGTH_SHORT).show()
            firebaseGoogleAuth(acc)
        }
        catch (e: ApiException){
            Toast.makeText(this@MainActivity, "Failed To Sign In", Toast.LENGTH_SHORT).show()
            Log.e("TESTING", "handleSignInResult: ",e )
            firebaseGoogleAuth(null)
        }
    }

    private fun firebaseGoogleAuth(acc: GoogleSignInAccount?) {
        val authCredential: AuthCredential = GoogleAuthProvider.getCredential(acc?.idToken,null)
        mAuth.signInWithCredential(authCredential).addOnCompleteListener(this
        ) { task ->
            if (task.isSuccessful) {
                val account: GoogleSignInAccount? = GoogleSignIn.getLastSignedInAccount(applicationContext)
                if(account != null){
                    val intent = Intent(this@MainActivity, DashBoardActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            } else {
                Toast.makeText(this@MainActivity, "Failed", Toast.LENGTH_SHORT).show()
            }
        }
    }
}