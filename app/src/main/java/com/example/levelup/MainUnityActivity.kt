package com.example.levelup

import android.content.Intent
import android.os.Bundle
import com.example.levelup.game.OverrideUnityActivity
import com.example.levelup.game.SendDataActivity

class MainUnityActivity : OverrideUnityActivity() {
        private var messages: MutableList<String> = mutableListOf()

        // Setup activity layout
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            val intent: Intent = intent
            handleIntent(intent)
        }

        override fun onNewIntent(intent: Intent?) {
            super.onNewIntent(intent)
            handleIntent(intent)
            setIntent(intent)
        }

        private fun handleIntent(intent: Intent?) {
            val extraGamePick = getString(R.string.extra_game_choice)
            if (intent == null || intent.extras == null) return

            if (intent.extras!!.containsKey(extraGamePick)) {
                val gamePick = intent.extras!!.getString(extraGamePick)
                SendMessage("GameSetter", "AndroidSetGame", gamePick)
            }

            if (intent.extras!!.containsKey("doQuit")) if (mUnityPlayer != null) {
                finish()
            }
        }

        override fun showMainActivity(setToColor: String?) {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_SINGLE_TOP
            intent.putExtra("setColor", setToColor)
            startActivity(intent)
        }

        override fun showSendDataActivity(score: Int) {
            val intent = Intent(this, SendDataActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_SINGLE_TOP
            intent.putExtra("Score", score)
            startActivity(intent)
        }

        override fun addMessage(message: String) {
            messages.add(message)
        }

        override fun showDashBoardActivity() {
            val intent = Intent(this, DashBoardActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_SINGLE_TOP
            startActivity(intent)
        }

        override fun onUnityPlayerUnloaded() {
            showDashBoardActivity()
        }
}