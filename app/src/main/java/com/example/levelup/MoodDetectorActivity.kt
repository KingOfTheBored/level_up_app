package com.example.levelup

import android.R.id.message
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.chaquo.python.Python
import com.example.levelup.TwitterScraper.Tweet
import com.example.levelup.TwitterScraper.TwitterScraper
import com.example.levelup.databinding.ActivityMoodDetectorBinding
import com.example.levelup.helper.Helper
import com.google.firebase.functions.FirebaseFunctionsException
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class MoodDetectorActivity : AppCompatActivity() {
    private lateinit var mTwitterScraper: TwitterScraper
    private lateinit var mTweet: String
    private lateinit var mMood: String
    private val TAG: String = "MoodDetectorActivity"

    private var _binding: ActivityMoodDetectorBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMoodDetectorBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mTwitterScraper = TwitterScraper()

        val twitterId = Helper.readUserSettings(this,
            getString(R.string.twitter_account_id),
            getString(R.string.default_twitter_account_id))

        mTwitterScraper.changeTwitterId(twitterId)

        binding.btnNlp.isEnabled = false
        binding.btnNlp.setOnClickListener {runNlp()}

        binding.btnUseDetectedMood.isEnabled = false
        binding.btnUseDetectedMood.setOnClickListener { returnMood() }

        binding.btnScanTweets.setOnClickListener {scanTweets()}

        binding.progBarRetrieveTweets.visibility = View.GONE
    }

    // Buttons Functions
    private fun scanTweets(){
        binding.progBarRetrieveTweets.visibility = View.VISIBLE
        binding.txtMoodDetector.visibility = View.INVISIBLE
        binding.btnScanTweets.isEnabled = false
        var tweets: ArrayList<Tweet>?
        mTwitterScraper.callFireBaseFunctionScrapeTweets()
            .addOnCompleteListener { task ->
                if (!task.isSuccessful) {
                    val e = task.exception
                    if (e is FirebaseFunctionsException) {
                        val code = e.code
                        val details = e.details
                        Log.e(TAG,
                            "Code: "+code+"\n"
                                    +"Details: "+details)
                    }
                }

                tweets = task.result
                val tweetsAcc: String

                if(tweets?.size != 0){
                    tweetsAcc = tweets?.get(0)!!.text
                    binding.txtMoodDetector.visibility = View.VISIBLE
                    mTweet = tweetsAcc
                    binding.btnNlp.isEnabled = true

                    // saves last received tweets in settings
                    Helper.writeUserSettings(this,
                        getString(R.string.last_tweet_id),
                        mTwitterScraper.getLastTwitterId())
                }else{
                    tweetsAcc = "No Tweets Available"
                }

                binding.btnScanTweets.isEnabled = true
                binding.progBarRetrieveTweets.visibility = View.GONE
                binding.txtMoodDetector.visibility = View.VISIBLE
                binding.txtMoodDetector.text = tweetsAcc
            }
    }

    private fun runNlp(){
        if (mTweet.isNotEmpty()) {
            binding.progBarRetrieveTweets.visibility = View.VISIBLE
            binding.txtMoodDetector.visibility = View.INVISIBLE

            GlobalScope.launch { // launch a new coroutine in background and continue
                val py = Python.getInstance()
                val module = py.getModule("Emotion Detector")
                val classifyResult = module.callAttr("detectEmotion", mTweet).toString()
                mMood = classifyResult
                binding.txtResult.text = classifyResult

                binding.progBarRetrieveTweets.visibility = View.INVISIBLE

                runOnUiThread {
                    binding.txtMoodDetector.visibility = View.VISIBLE
                    binding.btnUseDetectedMood.isEnabled = true
                }
            }
        }
    }

    private fun returnMood(){
        val intent = Intent()
        intent.putExtra("DetectedMood", mMood)
        setResult(5, intent)
        finish()
    }
}