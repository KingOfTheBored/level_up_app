package com.example.levelup

import com.example.levelup.TwitterScraper.TwitterScraper
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.levelup.TwitterScraper.TwitterUser
import com.example.levelup.adapter.TwitterUserListAdapter
import com.example.levelup.helper.Helper
import com.google.firebase.functions.FirebaseFunctionsException

class SearchTwitterAccountActivity : AppCompatActivity() {
    private lateinit var mBtnSearchUser: Button
    private lateinit var mTxtInUsername: EditText
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter: TwitterUserListAdapter
    private lateinit var mLayoutManager: RecyclerView.LayoutManager

    private val TAG = "SearchTwitterAccountAct"

    private var mTwitterScraper = TwitterScraper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_twitter_account)

        mTxtInUsername = findViewById(R.id.txtInUsername)
        mTxtInUsername.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                mBtnSearchUser.isEnabled = !s.isNullOrBlank() && s.isNotEmpty()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        mBtnSearchUser = findViewById(R.id.btnSearchUser)
        mBtnSearchUser.isEnabled = false
        mBtnSearchUser.setOnClickListener{searchUser()}

        mRecyclerView = findViewById(R.id.rvTwitterUsers)
        mLayoutManager = LinearLayoutManager(this)
        mRecyclerView.layoutManager = mLayoutManager
    }

    private fun onUserClick(twitterUser:TwitterUser) {
        mTwitterScraper.changeTwitterId(twitterUser.id)
        Helper.writeUserSettings(this,
            getString(R.string.twitter_account_id),
            twitterUser.id)
        Helper.writeUserSettings(this,
            getString(R.string.twitter_account_photo),
            twitterUser.profileImageUrl)
        Helper.writeUserSettings(this,
            getString(R.string.twitter_account_name),
            twitterUser.name)
        Helper.writeUserSettings(this,
            getString(R.string.twitter_account_handle),
            twitterUser.screenName)

        Log.i("TwitterID", twitterUser.id)
        val confirmation = "Twitter Account Selected"
        Toast.makeText(this, confirmation, Toast.LENGTH_SHORT).show()
    }

    private fun searchUser(){
        mBtnSearchUser.isEnabled = false
        val username = mTxtInUsername.text.toString()
        mTwitterScraper
            .callFireBaseFunctionSearchTwitterUsers(username)
            .addOnCompleteListener { task ->
                if (!task.isSuccessful) {
                    val e = task.exception
                    if (e is FirebaseFunctionsException) {
                        val code = e.code
                        val details = e.details
                        Log.e(TAG,
                            "Code: "+code+"\n"
                                    +"Details: "+details)
                    }
                }
                // On successful firebase function do this
                mBtnSearchUser.isEnabled = true
                if(!task.result.isNullOrEmpty()){
                    mAdapter =
                        TwitterUserListAdapter(task.result!!, this)
                    mRecyclerView.adapter = mAdapter
                    mAdapter.onItemClick = {twitterUser ->
                        onUserClick(twitterUser)
                        finish()
                    }
                }
            }
    }
}