package com.example.levelup.TwitterScraper

data class Tweet(val text:String, val id: String, val createdAt: String)