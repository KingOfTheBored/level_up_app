package com.example.levelup.TwitterScraper

import com.google.android.gms.tasks.Task
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase

class TwitterScraper {
    companion object {
        private var mTwitterId: String = "2244994945"
        private var mLastTweetId: String = ""
    }

    private var functions: FirebaseFunctions = Firebase.functions

    fun callFireBaseFunctionScrapeTweets(): Task<ArrayList<Tweet>> {
        // TODO: Change string argument to user's number
        val data = hashMapOf(
            "twitter_id" to mTwitterId,
            "push" to true
        )
        return functions
            .getHttpsCallable("scrapeTwitter")
            .call(data)
            .continueWith { task ->
                // Exceptions are propagated down
                val result = task.result?.data as Map<String, Any>
                val tweetTexts = result["tweetTexts"] as ArrayList<Map<String,String>>
                val tweets: ArrayList<Tweet> = ArrayList()
                tweetTexts.forEach{
                    it["text"]?.let { text ->
                        it["id"]?.let{ id ->
                            it["created_at"]?.let{ date ->
                                val tweet = Tweet(text,id,date)
                                tweets.add(tweet)
                            }
                        }
                    }
                }

                if (tweets.size>0){
                    mLastTweetId = tweets[0].id
                }

                tweets
            }
    }

    fun callFireBaseFunctionSearchTwitterUsers (username: String): Task<List<TwitterUser>> {
        val data = hashMapOf(
            "username" to username,
            "push" to true
        )
        return functions
            .getHttpsCallable("searchTwitterUsers ")
            .call(data)
            .continueWith { task ->
                val result = task.result?.data as Map<String, Any>
                val resultUsers = result["userAccounts"] as ArrayList<Map<String, Any>>
                val twitterUsers = resultUsers.map { user ->
                    var id = "ERROR"
                    var name = "ERROR"
                    var screenName = "ERROR"
                    var profileImageUrl = "ERROR"
                    var verified = false
                    user["id"]?.let { id = it as String }
                    user["name"]?.let { name = it as String }
                    user["screenName"]?.let { screenName = "@"+it as String }
                    user["profileImageUrl"]?.let { profileImageUrl = it as String }
                    user["verified"]?.let { verified = it as Boolean }

                    TwitterUser(id, name, screenName, profileImageUrl, verified)
                }

                twitterUsers
            }
    }

    fun changeTwitterId(twitterId: String){
        mTwitterId = twitterId
    }

    fun getLastTwitterId(): String{
        return mLastTweetId
    }
}