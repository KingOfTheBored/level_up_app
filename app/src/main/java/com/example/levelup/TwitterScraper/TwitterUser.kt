package com.example.levelup.TwitterScraper

class TwitterUser(
    val id: String,
    val name: String,
    val screenName: String,
    val profileImageUrl: String,
    val verified: Boolean)