package com.example.levelup.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.example.levelup.R
import com.example.levelup.habitManager.Day

class DayGridAdapter(private val days: List<Day>, private val current: Int, val context: Context): BaseAdapter(){
    override fun getCount(): Int {
        return days.size
    }

    override fun getItemId(position: Int): Long {
        return days[position].id.toLong()
    }

    override fun getItem(position: Int): Any? {
        return days[position]
    }

    override fun getView(
        position: Int,
        convertView: View?,
        parent: ViewGroup?
    ): View? {
        var view = convertView
        val dayNo: Int = days[position].dayNumber+1
        val actionNo: Int = days[position].noOfActions

        // 2
        if (view == null) {
            val layoutInflater  = LayoutInflater.from(context)
            view = layoutInflater.inflate(R.layout.day_square, parent, false)
        }

        // 3
        val mTxtDay: TextView = view!!.findViewById(R.id.txtDayNumber)
        val mCardDay: CardView = view.findViewById(R.id.cardDay)

        mTxtDay.text = dayNo.toString()
        when {
            dayNo>current+1 -> {
                mCardDay.setCardBackgroundColor(Color.GRAY)
            }
            actionNo>0 -> {
                mCardDay.setCardBackgroundColor(Color.GREEN)
            }
            dayNo==current+1 -> {
                mCardDay.setCardBackgroundColor(Color.BLUE)
            }
            else -> {
                mCardDay.setCardBackgroundColor(Color.RED)
            }
        }

        return view
    }
}