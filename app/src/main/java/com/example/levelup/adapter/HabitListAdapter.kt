package com.example.levelup.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.levelup.R
import com.example.levelup.habitManager.Habit

class HabitListAdapter : ListAdapter<Habit, HabitListAdapter.HabitViewHolder>(
    HabitComparator()
) {
    var onItemClick: ((Habit) -> Unit)? = null
    var onButtonClick: ((Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HabitViewHolder {
        return HabitViewHolder.create(
            parent
        )
    }

    override fun onBindViewHolder(holder: HabitViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current.name)
        holder.itemView.setOnClickListener {
            onItemClick?.invoke(current)
        }
    }

    class HabitViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val mHabitItemView: TextView = itemView.findViewById(R.id.txtHabitPreview)

        fun bind(text: String?) {
            mHabitItemView.text = text
        }

        companion object {
            fun create(parent: ViewGroup): HabitViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.recyclerview_item, parent, false)
                return HabitViewHolder(
                    view
                )
            }
        }
    }

    class HabitComparator : DiffUtil.ItemCallback<Habit>() {
        override fun areItemsTheSame(oldItem: Habit, newItem: Habit): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Habit, newItem: Habit): Boolean {
            return oldItem.habitId == newItem.habitId
        }
    }
}