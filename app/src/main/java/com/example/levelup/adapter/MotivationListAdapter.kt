package com.example.levelup.adapter

import android.view.View
import android.widget.AdapterView

class MotivationListAdapter: AdapterView.OnItemSelectedListener {
    private var _motivationScore = 0
    val motivationScore get() = _motivationScore

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
        _motivationScore = pos+1
    }

}