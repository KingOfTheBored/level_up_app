package com.example.levelup.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.levelup.R
import com.example.levelup.game.PlayerScore

class PlayerScoreListAdapter(private val playerScores: List<PlayerScore>, private val uid: String, val context: Context): RecyclerView.Adapter<PlayerScoreListAdapter.ViewHolder>(){

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val rank: TextView
        val username: TextView
        val score: TextView
        val card: CardView
        init {
            rank = view.findViewById(R.id.txtRank)
            username = view.findViewById(R.id.txtPlayerUsername)
            score = view.findViewById(R.id.txtPlayerScore)
            card = view.findViewById(R.id.cardScore)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.scoreboard_item, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val playerScore = playerScores[position]
        val rank = position+1
        val username = playerScore.username
        val score = playerScore.score
        val yours = (playerScore.uid == uid)
        viewHolder.rank.text = rank.toString()
        viewHolder.username.text = username
        viewHolder.score.text = score.toString()
        if(yours){
            viewHolder.card.setCardBackgroundColor(Color.GREEN)
        }
    }

    override fun getItemCount() = playerScores.size
}