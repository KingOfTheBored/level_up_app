package com.example.levelup.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.levelup.R
import com.example.levelup.TwitterScraper.TwitterUser

class TwitterUserListAdapter(val twitterUsers: List<TwitterUser>, val context: Context): RecyclerView.Adapter<TwitterUserListAdapter.ViewHolder>(){
    var onItemClick: ((TwitterUser) -> Unit)? = null

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val twitterName: TextView
        val twitterHandle: TextView
        val verified: ImageView
        val profilePic: ImageView
        init {
            view.setOnClickListener {
                onItemClick?.invoke(twitterUsers[adapterPosition])
            }

            twitterName = view.findViewById(R.id.txtTwitterName)
            twitterHandle = view.findViewById(R.id.txtTwitterHandle)
            verified = view.findViewById(R.id.imgVerified)
            profilePic = view.findViewById(R.id.imgTwitterProfilePic)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.twitter_user_list, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val twitterUser = twitterUsers[position]
        viewHolder.twitterName.text = twitterUser.name
        viewHolder.twitterHandle.text = twitterUser.screenName
        viewHolder.verified.visibility = if (twitterUser.verified) View.VISIBLE else View.INVISIBLE
        Glide.with(context).load(twitterUser.profileImageUrl).into(viewHolder.profilePic)
    }

    override fun getItemCount() = twitterUsers.size
}