package com.example.levelup.game

import android.content.Context
import android.util.Log
import com.chaquo.python.Python
import com.example.levelup.R

object GamePicker {
    private const val noOfGames: Int = 3

    fun pickGame(context: Context?, gameData: GamePickerData ): String{
/*        return when((0 until noOfGames).random()){
            0-> context!!.getString(R.string.game_choice_2)
            1-> context!!.getString(R.string.game_choice_3)
            2-> context!!.getString(R.string.game_choice_4)
            else -> {
                Log.e("GamePicker", "Game index not selected")
                return ""
            }
        }*/
        val py = Python.getInstance()
        val module = py.getModule("MachineLearning")
        val game0 = context!!.getString(R.string.game_choice_2)
        val game1 = context.getString(R.string.game_choice_3)
        val game2 = context.getString(R.string.game_choice_4)

        val games = listOf(game0, game1, game2)
        val choice = module.callAttr("pickGame",
            games, gameData.age, gameData.automaticity,
            gameData.currentStreak, gameData.difficultyLevel.toString(),
            gameData.duration.toString(), gameData.longestStreak, gameData.mood,
            gameData.motivation, gameData.time, gameData.type.toString()).toString()

        return choice
    }

}