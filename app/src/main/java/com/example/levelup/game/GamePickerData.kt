package com.example.levelup.game

import com.example.levelup.habitManager.DifficultyLevel
import com.example.levelup.habitManager.HabitDuration
import com.example.levelup.habitManager.HabitType
import com.google.firebase.database.Exclude

data class GamePickerData(
    var mood: String? = "",
    var gameChoice: String? = "",
    var age: Int = 0,
    var currentStreak: Int = 0,
    var longestStreak: Int = 0,
    var motivation: Int = 0,
    var automaticity: Int = 0,
    var time: Long = 0,
    var duration: HabitDuration = HabitDuration.FIVE_MIN,
    var difficultyLevel: DifficultyLevel = DifficultyLevel.TRIVIAL,
    var type: HabitType = HabitType.POSITIVE,
    val motivationAfter: Int,
) {

    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "mood" to mood,
            "gameChoice" to gameChoice,
            "age" to age,
            "time" to time,
            "automaticity" to automaticity,
            "currentStreak" to currentStreak,
            "longestStreak" to longestStreak,
            "motivation" to motivation,
            "duration" to duration,
            "difficultyLevel" to difficultyLevel,
            "type" to type,
            "motivationAfter" to motivationAfter
        )
    }

}

/*

time
automaticity*/