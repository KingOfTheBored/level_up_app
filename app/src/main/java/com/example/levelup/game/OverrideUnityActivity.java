package com.example.levelup.game;
import android.os.Bundle;

import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

public abstract class OverrideUnityActivity extends UnityPlayerActivity
{
    public static OverrideUnityActivity instance = null;

    abstract protected void showMainActivity(String setToColor);
    abstract protected void showSendDataActivity(int score);
    abstract protected void addMessage(String color);
    abstract protected void showDashBoardActivity();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        instance = this;
    }

    protected void SendMessage(String s, String s1, String s2){
        UnityPlayer.UnitySendMessage(s, s1, s2);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        instance = null;
    }
}
