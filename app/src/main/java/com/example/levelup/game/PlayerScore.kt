package com.example.levelup.game

data class PlayerScore(val uid: String, val username: String, val score: Long) {
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "uid" to uid,
            "username" to username,
            "score" to score
        )
    }
}