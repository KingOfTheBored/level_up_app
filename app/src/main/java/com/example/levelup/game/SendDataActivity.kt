package com.example.levelup.game

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.example.levelup.*
import com.example.levelup.adapter.MotivationListAdapter
import com.example.levelup.databinding.ActivitySendDataBinding
import com.example.levelup.habitManager.Day
import com.example.levelup.helper.Helper
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import java.util.concurrent.TimeUnit

class SendDataActivity : AppCompatActivity() {
    private lateinit var mMotivationListAdapter: MotivationListAdapter
    private val habitViewModel: HabitViewModel by viewModels {
        HabitViewModelFactory((this.application as HabitsApplication).repository)
    }
    private val dayViewModel: DayViewModel by viewModels {
        DayViewModelFactory((this.application as HabitsApplication).dayRepository)
    }
    private var _binding: ActivitySendDataBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivitySendDataBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val score = intent.getIntExtra("Score", 0)

        val previousScore = Helper
            .readUserSettings(this, getString(R.string.score),"0")
            .toInt()

        val newScore = previousScore+score
        binding.txtScore.text = "Score: $newScore"

        Helper
            .writeUserSettings(this, getString(R.string.score), newScore.toString())

        binding.btnReturn.visibility = View.GONE
        binding.textViewtitleThankYouMsg.visibility = View.GONE
        binding.btnSendData.setOnClickListener {
            sendData()
        }
        binding.btnReturn.setOnClickListener {
            backToApp()
        }
        binding.txtMotivationAfter.text = getString(R.string.motivation_after)

        // Motivation Spinner
        ArrayAdapter.createFromResource(
            this,
            R.array.motivation_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.spinnerMotivationAfter.adapter = adapter
            mMotivationListAdapter = MotivationListAdapter()
            binding.spinnerMotivationAfter.onItemSelectedListener  = mMotivationListAdapter
        }

        sendScore(newScore.toLong())
    }

    private fun sendData(){
        binding.btnSendData.isEnabled = false
        val database = Firebase.database
        val myDatabase = database.getReference("message")

        // replace with input motivation
        val motivationScore = Helper.readUserSettings(this, getString(R.string.picked_motivation),"-1").toInt()
        val mood = Helper.readUserSettings(this, getString(R.string.picked_mood),"N/A")
        val time: Long = System.currentTimeMillis() % (1000*60*60*24)
        val gamePick = Helper.readUserSettings(this, getString(R.string.picked_game),"N/A")
        val automaticity = Helper.readUserSettings(this, getString(R.string.picked_automaticity),"-1").toInt()
        val motivationAfter = mMotivationListAdapter.motivationScore

        val data = GamePickerData(mood,
            motivation = motivationScore,
            gameChoice = gamePick,
            automaticity = automaticity,
            time = time,
            motivationAfter = motivationAfter)

        habitViewModel.allHabits.observe(this, Observer {habitsList ->
            for(habit in habitsList){
                val habitAge = TimeUnit.MILLISECONDS
                    .toDays(habit.calculateAge())
                    .toInt()

                val dayObserver = dayViewModel.daysByHabit(habit.habitId)
                dayObserver.observe(this, Observer {daysList ->
                    data.apply {
                        age = habitAge
                        currentStreak = Day.currentStreak(daysList, habitAge)
                        longestStreak = Day.longestStreak(daysList)
                        difficultyLevel = habit.difficulty
                        duration = habit.duration
                        type = habit.type
                    }

                    val key = myDatabase.child("posts").push().key
                    if (key == null) {
                        Log.e("sendData", "Couldn't get push key for posts")
                        backToApp()
                    }

                    val dataValues = data.toMap()
                    val childUpdates = hashMapOf<String, Any>(
                        "/data/$key" to dataValues
                    )

                    myDatabase.updateChildren(childUpdates)
                })
            }
            binding.textViewtitleThankYouMsg.visibility = View.VISIBLE
            binding.btnReturn.visibility = View.VISIBLE
            binding.spinnerMotivationAfter.visibility = View.GONE
            binding.btnSendData.visibility = View.GONE
            binding.txtMotivationAfter.visibility = View.GONE
        })
    }

    private fun backToApp(){
        finish()
    }

    private fun sendScore(newScore: Long){
        val currentUser = FirebaseAuth.getInstance().currentUser!!
        val uid = currentUser.uid
        val username = currentUser.displayName!!

        val database = Firebase.database
        val myDatabase = database.reference
            .child("scores")

        val myScore = myDatabase
            .orderByChild("uid")
            .equalTo(uid)
            .limitToFirst(1)

        myScore.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // only listen once
                myScore.removeEventListener(this)

                var key = myDatabase.push().key
                if (dataSnapshot.exists()) {
                    for (child in dataSnapshot.children) {
                        key = child.key
                    }
                    Log.e("value", dataSnapshot.value.toString())
                }

                if (key == null) {
                    Log.e("sendScore", "Couldn't get push key for scores")
                }else{
                    val dataValues = PlayerScore(uid, username, newScore).toMap()
                    val childUpdates = hashMapOf<String, Any>(
                        "/$key" to dataValues
                    )

                    myDatabase.updateChildren(childUpdates)
                }
            }
        })
    }
}