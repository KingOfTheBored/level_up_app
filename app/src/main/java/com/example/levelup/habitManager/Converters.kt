package com.example.levelup.habitManager

import androidx.room.TypeConverter

class Converters {

    @TypeConverter
    fun toDifficultyLevel(value: String) = enumValueOf<DifficultyLevel>(value)

    @TypeConverter
    fun fromDifficultyLevel(value: DifficultyLevel) = value.name

    @TypeConverter
    fun toHabitDuration(value: String) = enumValueOf<HabitDuration>(value)

    @TypeConverter
    fun fromHabitDuration(value: HabitDuration) = value.name

    @TypeConverter
    fun toHabitType(value: String) = enumValueOf<HabitType>(value)

    @TypeConverter
    fun fromHabitType(value: HabitType) = value.name
}