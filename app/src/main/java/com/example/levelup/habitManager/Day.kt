package com.example.levelup.habitManager

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey

@Entity(tableName = "day_library",
    foreignKeys = [
        ForeignKey(entity = Habit::class,
            parentColumns = ["habitId"],
            childColumns = ["habit"],
            onDelete = CASCADE)])
class Day (habit: Int, dayNumber: Int, noOfActions: Int){
    @PrimaryKey(autoGenerate = true)
    var id = 0

    @ColumnInfo(name = "day_number")
    var dayNumber = 0

    @ColumnInfo(name = "no_of_actions")
    var noOfActions = 0

    @ColumnInfo(name = "habit", index = true)
    var habit= 0

    init {
        this.habit = habit
        this.dayNumber = dayNumber
        this.noOfActions = noOfActions
    }


    companion object{
        fun longestStreak(days: List<Day>): Int{
            var streak = 0
            var longestStreak = 0

            for (day in days){
                if (day.noOfActions>0){
                    streak++
                    if (streak>longestStreak){
                        longestStreak = streak
                    }
                }else{
                    streak = 0
                }
            }

            return longestStreak
        }

        fun currentStreak(days: List<Day>, currentDay: Int): Int{
            val reversed = days.reversed()
            var currentStreak = 0
            for (day in reversed){
                if (day.noOfActions>0){
                    currentStreak++
                }else if (day.dayNumber == currentDay){
                    continue
                }else{
                    return currentStreak
                }
            }

            return currentStreak
        }
    }
}