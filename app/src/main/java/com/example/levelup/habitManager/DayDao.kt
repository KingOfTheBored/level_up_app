package com.example.levelup.habitManager

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface DayDao {
    @Query("SELECT * FROM day_library WHERE habit = :habitId")
    fun retrieveHabitDays(habitId: Int): Flow<List<Day>>

    @Query("SELECT * FROM day_library WHERE id = :id")
    fun retrieveHabitById(id: Int): Flow<Day>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertDay(day: Day)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun updateDay(day: Day)

    @Delete
    suspend fun delete(day: Day)

    @Query("SELECT * FROM day_library WHERE habit = :habitId ORDER BY id DESC LIMIT 1")
    fun retrieveLastDay(habitId: Int): Flow<Day>

    @Query("SELECT * FROM day_library WHERE habit = :habitId AND day_number = :dayNo LIMIT 1")
    fun retrieveDay(habitId: Int, dayNo: Int): Flow<Day>
}