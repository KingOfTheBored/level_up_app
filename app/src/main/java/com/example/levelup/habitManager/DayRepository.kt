package com.example.levelup.habitManager

import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow

class DayRepository internal constructor(private val dayDao: DayDao)
{
    fun allHabitDays(id:Int): Flow<List<Day>> {
        return dayDao.retrieveHabitDays(id)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(day: Day) {
        dayDao.insertDay(day)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun update(day: Day) {
        dayDao.updateDay(day)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun delete(day: Day) {
        dayDao.delete(day)
    }

    fun habitLastDay(habitId: Int): Flow<Day> {
        return dayDao.retrieveLastDay(habitId)
    }

    fun habitDay(habitId: Int, dayNo: Int): Flow<Day> {
        return dayDao.retrieveDay(habitId, dayNo)
    }
}