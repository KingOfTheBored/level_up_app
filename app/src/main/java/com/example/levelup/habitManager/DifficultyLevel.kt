package com.example.levelup.habitManager

enum class DifficultyLevel(private val description: String) {
    TRIVIAL("Trivial"),
    EASY("Easy"),
    MEDIUM("Medium"),
    HARD("Hard");

    override fun toString(): String {
        return description
    }
}