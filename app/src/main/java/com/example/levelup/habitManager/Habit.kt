package com.example.levelup.habitManager

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "habit_library")
class Habit(name: String,
            type: HabitType,
            difficulty: DifficultyLevel,
            duration: HabitDuration,
            cue: String = "",
            motivation: String = "",
            reminder: String = "",
            reminderId: Int = -1
) {
    @PrimaryKey(autoGenerate = true)
    var habitId: Int = 0

    @ColumnInfo(name = "start_date")
    var startDate: Long

    @ColumnInfo(name = "strength")
    var strength: Double

    @ColumnInfo(name = "name")
    var name: String

    @ColumnInfo(name = "cue")
    var cue: String

    @ColumnInfo(name = "motivation")
    var motivation: String

    @ColumnInfo(name = "reminder")
    var reminder: String

    @ColumnInfo(name = "reminder_id")
    var reminderId: Int = -1

    @ColumnInfo(name = "type")
    var type: HabitType

    @ColumnInfo(name = "difficulty")
    var difficulty: DifficultyLevel

    @ColumnInfo(name = "duration")
    var duration: HabitDuration

    init {
        val now = Date()
        this.name = name
        this.type = type
        this.difficulty = difficulty
        this.duration = duration
        this.cue = cue
        this.motivation = motivation
        this.reminder = reminder
        this.reminderId = reminderId
        strength = 0.0
        startDate = now.time
    }

    fun calculateAge(): Long {
        val now = Date()
        return now.time - startDate
    }

    override fun toString(): String {
        val date = Date(startDate)
        return """
            Habit ID: $habitId
            Name: $name
            Start Date: $date
            Age: ${calculateAge()}
            Strength: $strength

            """.trimIndent()
    }
}