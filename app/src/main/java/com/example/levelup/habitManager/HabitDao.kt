package com.example.levelup.habitManager

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface HabitDao {
    @Query("SELECT * FROM habit_library")
    fun retrieveHabits(): Flow<List<Habit>>

    @Query("SELECT * FROM habit_library WHERE habitId = :id")
    fun retrieveHabitById(id: Int): Flow<Habit>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertHabit(habit: Habit)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun updateHabit(habit: Habit)

    @Delete
    suspend fun deleteHabit(habit: Habit)
}