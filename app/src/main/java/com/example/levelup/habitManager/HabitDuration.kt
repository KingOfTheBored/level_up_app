package com.example.levelup.habitManager

enum class HabitDuration (private val description: String) {
    FIVE_MIN("Five mins or less"),
    FIFTEEN_MIN("Fifteen mins or less"),
    HOUR("Hour or less"),
    OVER_HOUR("Over an hour");

    override fun toString(): String {
        return description
    }
}