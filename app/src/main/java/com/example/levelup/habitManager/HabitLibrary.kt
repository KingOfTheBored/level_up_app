package com.example.levelup.habitManager

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = [Habit::class, Day::class], version = 3, exportSchema = false)
@TypeConverters(Converters::class)
abstract class HabitLibrary : RoomDatabase() {

    abstract fun habitDoa(): HabitDao
    abstract fun dayDoa(): DayDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: HabitLibrary? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): HabitLibrary {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    HabitLibrary::class.java,
                    "habit_library"
                )
                    .fallbackToDestructiveMigration()
                    .addCallback(HabitDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }

    private class HabitDatabaseCallback(
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                scope.launch {
                    populateDatabase(database.habitDoa())
                }
            }
        }

        suspend fun populateDatabase(habitDao: HabitDao) {
            //Can use to populate database
        }
    }
}