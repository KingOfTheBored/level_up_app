package com.example.levelup.habitManager

import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow

class HabitRepository internal constructor(private val habitDao: HabitDao) {
    // Room executes all queries on a separate thread.
    // Observed Flow will notify the observer when the data has changed.
    val allHabits: Flow<List<Habit>> = habitDao.retrieveHabits()

    fun habitById(id:Int): Flow<Habit>{
        return habitDao.retrieveHabitById(id)
    }

    // By default Room runs suspend queries off the main thread, therefore, we don't need to
    // implement anything else to ensure we're not doing long running database work
    // off the main thread.
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(habit: Habit) {
        habitDao.insertHabit(habit)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun update(habit: Habit) {
        habitDao.updateHabit(habit)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun delete(habit: Habit) {
        habitDao.deleteHabit(habit)
    }
}