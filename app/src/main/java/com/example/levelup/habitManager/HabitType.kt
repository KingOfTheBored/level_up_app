package com.example.levelup.habitManager

enum class HabitType (private val description: String) {
    POSITIVE("Positive"),
    NEGATIVE("Negative");

    override fun toString(): String {
        return description
    }
}