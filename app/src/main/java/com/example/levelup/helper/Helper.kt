package com.example.levelup.helper

import android.content.Context
import android.util.Log
import com.example.levelup.R
import com.google.firebase.auth.FirebaseAuth
import java.util.*

class Helper {
    companion object{
        fun writeUserSettings(context: Context?, key: String, stringVal: String){
            val sharedPreferences = context?.getSharedPreferences(
                R.string.user_settings_file_id.toString(),
                Context.MODE_PRIVATE) ?: return

            with (sharedPreferences.edit()) {
                putString(key, stringVal)
                apply()
            }
        }

        fun readUserSettings(context:Context?, key: String, defaultValue: String): String{
            val sharedPreferences = context?.getSharedPreferences(
                R.string.user_settings_file_id.toString(),
                Context.MODE_PRIVATE)?: return defaultValue

            return sharedPreferences.getString(key, defaultValue)?: return defaultValue
        }

        fun getCurrentUserDayNo(): Long{
            val usersCreationTime = FirebaseAuth.getInstance()
                .currentUser!!
                .metadata!!
                .creationTimestamp

            val calCreateDate = Calendar.getInstance()
            calCreateDate.timeInMillis = usersCreationTime
            val createDate = getStartOfDay(calCreateDate)

            val calCurrent = Calendar.getInstance()
            val current = getStartOfDay(calCurrent)

            val usersDays = (current - createDate) / (1000*60*60*24)

            Log.i("Users Creation Time", usersDays.toString())
            return usersDays
        }

        private fun getStartOfDay(cal: Calendar): Long{
            val year = cal[Calendar.YEAR]
            val month = cal[Calendar.MONTH]
            val day = cal[Calendar.DATE]
            cal[year, month, day, 0, 0] = 0

            return cal.timeInMillis
        }
    }
}