package com.example.levelup.helper

import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Context
import android.widget.EditText
import android.widget.TimePicker
import java.util.*

class SetTime(
    private val editText: EditText,
    private val context: Context
) : OnTimeSetListener
{
    var pickedHour = 0
    var pickedMinute = 0
    var set = false
    private val myCalendar: Calendar = Calendar.getInstance()

    override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
        pickedHour = hourOfDay
        pickedMinute = minute
        set = true
        editText.setText(String.format("%02d", hourOfDay)+" : "+String.format("%02d", minute))
    }

    init {
        editText.setOnClickListener{
            val hour: Int = myCalendar.get(Calendar.HOUR_OF_DAY)
            val minute: Int = myCalendar.get(Calendar.MINUTE)
            TimePickerDialog(context, this, hour, minute, true).show()
        }
    }
}