package com.example.levelup.notifications

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.example.levelup.helper.SetTime
import java.util.*

object Notification {
    fun tapNotification(context:Context, time: Long, id: Int, msg: String){
        // Create an explicit intent for an Activity in your app
        val intent = Intent(context, ReminderBroadcast::class.java).apply {
            putExtra("message", msg)
            putExtra("id", id)
        }

        val pendingIntent: PendingIntent
                = PendingIntent.getBroadcast(context, id, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val alarmManager: AlarmManager
                = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        if (android.os.Build.VERSION.SDK_INT >= 19) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time, AlarmManager.INTERVAL_DAY, pendingIntent)
        } else {
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, time, AlarmManager.INTERVAL_DAY, pendingIntent)
        }
    }

    fun cancelNotification(context: Context, id: Int, msg: String){
        val intent = Intent(context, ReminderBroadcast::class.java).apply {
            putExtra("message", msg)
            putExtra("id", id)
        }

        val pendingIntent
                = PendingIntent.getBroadcast(context, id, intent, PendingIntent.FLAG_NO_CREATE)

        val alarmManager: AlarmManager
                = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        if (pendingIntent != null) {
            alarmManager.cancel(pendingIntent)
            Toast.makeText(context, "reminder removed", Toast.LENGTH_SHORT).show()
        }else{
            Toast.makeText(context, "no reminder found", Toast.LENGTH_SHORT).show()
        }

    }

    fun addReminder(context: Context, setTime: SetTime, id: Int, msg: String){
        val cal = Calendar.getInstance()
        val currentHour = cal.get(Calendar.HOUR_OF_DAY)
        val currentMinute = cal.get(Calendar.MINUTE)

        val hour = setTime.pickedHour
        val minute = setTime.pickedMinute
        if(currentHour>hour || (currentHour==hour && currentMinute>=minute)){
            cal.add(Calendar.DATE, 1)
        }

        cal.set(Calendar.HOUR_OF_DAY, hour)
        cal.set(Calendar.MINUTE, minute)

        /*val pattern = "yyyy-MM-dd (hh:mm)"
        val simpleDateFormat = SimpleDateFormat(pattern)
        val date: String = simpleDateFormat.format(cal.timeInMillis)*/

        val left = timeLeft(cal.timeInMillis - System.currentTimeMillis())

        Toast.makeText(context, "$left till next reminder", Toast.LENGTH_LONG)
            .show()

        tapNotification(context, cal.timeInMillis, id, msg)
    }

    private fun timeLeft(millSec: Long): String{
        val seconds = millSec / 1000 % 60
        val minutes = millSec / (1000 * 60) % 60
        val hours = millSec / (1000 * 60 * 60)
        val hourString = if (hours>0) "$hours hours," else ""
        val minuteString = if (minutes>0) "$minutes minutes," else ""
        val secondString = if (seconds>0) "$seconds, seconds" else ""
        return "$hourString $minuteString $secondString"
    }
}