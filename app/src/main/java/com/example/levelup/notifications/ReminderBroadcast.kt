package com.example.levelup.notifications

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.levelup.R

class ReminderBroadcast : BroadcastReceiver() {
    private val CHANNEL_ID = "Habit_Channel"

    override fun onReceive(context: Context, intent: Intent) {
            //val vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

            val msg = intent.getStringExtra("message") ?: "No message in intent"
            //val id = intent.getIntExtra("id", -1)
            val builder = NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_clipboard)
                .setContentTitle("Habit Reminder")
                .setContentText(msg)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .build()

            val notificationManager: NotificationManagerCompat =
                NotificationManagerCompat.from(context)

        notificationManager.notify(kotlin.random.Random.nextInt(10000), builder)
    }
}