package com.example.levelup.settings

import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.example.levelup.R

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
        val preference = preferenceManager.findPreference<Preference>("messages")
        preference?.fragment = SettingsTwitterFragment::class.java.name.removeSuffix("\$Companion")
    }
}