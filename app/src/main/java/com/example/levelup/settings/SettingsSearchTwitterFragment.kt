package com.example.levelup

import com.example.levelup.TwitterScraper.TwitterScraper
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.levelup.TwitterScraper.TwitterUser
import com.example.levelup.adapter.TwitterUserListAdapter
import com.example.levelup.databinding.FragmentSettingsSearchTwitterBinding
import com.example.levelup.databinding.FragmentSettingsTwitterBinding
import com.example.levelup.helper.Helper
import com.google.firebase.functions.FirebaseFunctionsException

class SettingsSearchTwitterFragment : Fragment() {
    private var _binding: FragmentSettingsSearchTwitterBinding? = null
    private val binding get() = _binding!!

    private val TAG = "SearchTwitterAccountAct"

    private var mTwitterScraper = TwitterScraper()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        _binding = FragmentSettingsSearchTwitterBinding.inflate(inflater,
            container,
            false)

        binding.txtInUsername.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                binding.btnSearchUser.isEnabled = !s.isNullOrBlank() && s.isNotEmpty()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })


        binding.btnSearchUser.isEnabled = false
        binding.btnSearchUser.setOnClickListener{searchUser()}
        binding.rvTwitterUsers.layoutManager = LinearLayoutManager(activity)

        return binding.root
    }

    private fun onUserClick(twitterUser:TwitterUser) {
        mTwitterScraper.changeTwitterId(twitterUser.id)
        Helper.writeUserSettings(activity,
            getString(R.string.twitter_account_id),
            twitterUser.id)
        Helper.writeUserSettings(activity,
            getString(R.string.twitter_account_photo),
            twitterUser.profileImageUrl)
        Helper.writeUserSettings(activity,
            getString(R.string.twitter_account_name),
            twitterUser.name)
        Helper.writeUserSettings(activity,
            getString(R.string.twitter_account_handle),
            twitterUser.screenName)

        Log.i("TwitterID", twitterUser.id)
        val confirmation = "Twitter Account Selected"
        Toast.makeText(activity, confirmation, Toast.LENGTH_SHORT).show()
        activity?.supportFragmentManager
            ?.popBackStack()
    }

    private fun searchUser(){
        binding.btnSearchUser.isEnabled = false
        binding.progBarSearchTwitterAccounts.visibility = View.VISIBLE
        val username = binding.txtInUsername.text.toString()
        mTwitterScraper
            .callFireBaseFunctionSearchTwitterUsers(username)
            .addOnCompleteListener { task ->
                if (!task.isSuccessful) {
                    val e = task.exception
                    if (e is FirebaseFunctionsException) {
                        val code = e.code
                        val details = e.details
                        Log.e(TAG,
                            "Code: "+code+"\n"
                                    +"Details: "+details)
                    }
                }
                binding.progBarSearchTwitterAccounts.visibility = View.GONE
                // On successful firebase function do this
                binding.btnSearchUser.isEnabled = true
                if(!task.result.isNullOrEmpty()){
                    val adapter =
                        TwitterUserListAdapter(task.result!!, requireContext())
                    binding.rvTwitterUsers.adapter = adapter
                    adapter.onItemClick = {twitterUser ->
                        onUserClick(twitterUser)

                    }
                }
            }
    }
}