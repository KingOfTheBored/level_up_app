package com.example.levelup.settings

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.levelup.R
import com.example.levelup.SettingsSearchTwitterFragment
import com.example.levelup.databinding.FragmentHomeBinding
import com.example.levelup.databinding.FragmentSettingsTwitterBinding
import com.example.levelup.helper.Helper

class SettingsTwitterFragment : Fragment() {
    private var _binding: FragmentSettingsTwitterBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSettingsTwitterBinding.inflate(inflater,
            container,
            false)

        binding.cardChangeAccount.setOnClickListener {
            switchToSearchFragment()
        }

        binding.cardRemoveAccount.setOnClickListener {
            removeTwitterAccount()
        }

        setTwitterSettingsView()

        return binding.root
    }

    companion object {
        fun newInstance() = SettingsTwitterFragment()
    }

    private fun switchToSearchFragment(){
        val fragment = SettingsSearchTwitterFragment()
        activity
            ?.supportFragmentManager
            ?.beginTransaction()
            ?.replace(R.id.nav_setting_fragment, fragment)
            ?.addToBackStack(null)
            ?.commit()
    }

    private fun removeTwitterAccount(){
        Helper.writeUserSettings(activity,
            getString(R.string.twitter_account_id),
            "")
        Helper.writeUserSettings(activity,
            getString(R.string.twitter_account_photo),
            "")
        Helper.writeUserSettings(activity,
            getString(R.string.twitter_account_name),
            "")
        Helper.writeUserSettings(activity,
            getString(R.string.twitter_account_handle),
            "")
        setTwitterSettingsView()
    }

    private fun setTwitterSettingsView(){
        val twitterId = Helper.readUserSettings(requireContext(), getString(R.string.twitter_account_id), "")
        if(twitterId==""){
            binding.titleCurrentAccount.visibility = View.GONE
            binding.ViewTwitterUser.visibility = View.GONE
            binding.txtSetAccount.text = "Add Account"
            binding.cardRemoveAccount.visibility = View.GONE
        }else{
            val photo = Helper.readUserSettings(activity,
                getString(R.string.twitter_account_photo),
                "")
            val accountName = Helper.readUserSettings(activity,
                getString(R.string.twitter_account_name),
                "")
            val handle = Helper.readUserSettings(activity,
                getString(R.string.twitter_account_handle),
                "")

            Glide.with(requireContext()).load(photo).into(binding.imageView2)
            binding.txtSettingTwitterName.text = accountName
            binding.txtSettingTwitterHandle.text = handle
            binding.txtSetAccount.text = "Change Account"
            binding.cardRemoveAccount.visibility = View.VISIBLE
        }
    }
}