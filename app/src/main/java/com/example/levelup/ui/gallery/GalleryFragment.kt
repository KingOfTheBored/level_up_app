package com.example.levelup.ui.gallery

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.levelup.*
import com.example.levelup.adapter.MotivationListAdapter
import com.example.levelup.databinding.FragmentGalleryBinding
import com.example.levelup.game.GamePicker
import com.example.levelup.game.GamePickerData
import com.example.levelup.habitManager.Day
import com.example.levelup.habitManager.Habit
import com.example.levelup.helper.Helper
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import java.util.concurrent.TimeUnit

class GalleryFragment : Fragment() , AdapterView.OnItemSelectedListener{

    private lateinit var mMotivationListAdapter: MotivationListAdapter
    private lateinit var mMoods: ArrayAdapter<CharSequence>
    private lateinit var galleryViewModel: GalleryViewModel
    private lateinit var mPickedMood: String
    private var _binding: FragmentGalleryBinding? = null
    private val binding get() = _binding!!
    private val habitViewModel: HabitViewModel by viewModels {
        HabitViewModelFactory((activity?.application as HabitsApplication).repository)
    }
    private val dayViewModel: DayViewModel by viewModels {
        DayViewModelFactory((activity?.application as HabitsApplication).dayRepository)
    }
    private lateinit var mHabit: Habit
    private var currentStreak: Int = 0
    private var longestStreak: Int = 0
    private var age: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        galleryViewModel =
            ViewModelProviders.of(this).get(GalleryViewModel::class.java)

        val sharedPreferences = context?.getSharedPreferences(
            R.string.user_settings_file_id.toString(),
            Context.MODE_PRIVATE)

        if (sharedPreferences != null) {
            galleryViewModel.setupPreferenceListener(sharedPreferences, getString(R.string.twitter_account_id), getString(R.string.ticket))
        }

        _binding = FragmentGalleryBinding.inflate(inflater, container, false)

        galleryViewModel.moodButtonEnabled.observe(viewLifecycleOwner, Observer {
            binding.btnDetectMood.isEnabled = it
        })
        galleryViewModel.moodText.observe(viewLifecycleOwner, Observer {
            binding.txtMood.text = it
        })
        galleryViewModel.motivationText.observe(viewLifecycleOwner, Observer {
            binding.txtMotivation.text = it
        })
        galleryViewModel.ticketText.observe(viewLifecycleOwner, Observer {
            binding.txtGameTicket.text = getString(R.string.game_ticket, it)
            val ticketNo = it.toInt()
            Log.i("TicketNo", ticketNo.toString())
            binding.btnLaunchGame.isEnabled = (ticketNo>0)
            binding.txtNoHabitWarning.visibility = if(ticketNo <= 0) View.VISIBLE else View.INVISIBLE
        })

        binding.btnDetectMood.setOnClickListener { launchMoodDetector() }
        binding.btnPickGame.setOnClickListener { chooseGame() }
        binding.btnLaunchGame.setOnClickListener{ launchGame() }

        // Mood Spinner
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.moods_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            binding.spinnerMood.adapter = adapter
            mMoods = adapter
        }
        binding.spinnerMood.onItemSelectedListener = this

        // Motivation Spinner
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.motivation_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.spinnerMotivation.adapter = adapter
            mMotivationListAdapter = MotivationListAdapter()
            binding.spinnerMotivation.onItemSelectedListener  = mMotivationListAdapter
        }

        // Automaticity Spinner
        val list = listOf(1,2,3,4,5,6,7,8,9,10)
        binding.spinnerAutomaticity.adapter = ArrayAdapter<Int>(
            requireContext(),
            android.R.layout.simple_spinner_item,
            list
        )

        // Game Choice Spinner
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.game_choices,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            binding.spinnerGamePick.adapter = adapter
        }

        habitViewModel.allHabits.observe(viewLifecycleOwner, Observer{habits ->
            if(habits.isNotEmpty()){
                mHabit = habits[0]
                val age = TimeUnit.MILLISECONDS
                    .toDays(mHabit.calculateAge())
                    .toInt()
                dayViewModel.daysByHabit(mHabit.habitId).observe(viewLifecycleOwner, Observer{days ->
                    longestStreak = Day.longestStreak(days)
                    currentStreak = Day.currentStreak(days, age)
                })
            }
        })

        return binding.root
    }

    private fun launchGame() {
        // Retrieve data for machine learning
        val automaticity = binding.spinnerAutomaticity.selectedItem.toString()
        val gameChoice = binding.spinnerGamePick.selectedItem.toString()
        Helper.writeUserSettings(activity, getString(R.string.picked_mood), mPickedMood)
        Helper.writeUserSettings(activity, getString(R.string.picked_motivation), mMotivationListAdapter.motivationScore.toString())
        Helper.writeUserSettings(activity, getString(R.string.picked_automaticity), automaticity)
        Helper.writeUserSettings(activity, getString(R.string.picked_game), gameChoice)

        // decrement ticket
        var ticket = Helper.readUserSettings(requireContext(), getString(R.string.ticket), "0").toInt()
        ticket--
        Helper.writeUserSettings(requireContext(), getString(R.string.ticket), ticket.toString())

        // sends user day data to firebase database
        addUserDay(automaticity.toInt(), mMotivationListAdapter.motivationScore, mPickedMood)

        // Starts game activity
        val launchGameIntent = Intent(activity, MainUnityActivity::class.java)
        launchGameIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT

        launchGameIntent.putExtra(
            getString(R.string.extra_game_choice),
            gameChoice)
        startActivityForResult(launchGameIntent, 1)
    }

    override fun onNothingSelected(p0: AdapterView<*>?) { }

    override fun onItemSelected(parent: AdapterView<*>, view: View?, pos: Int, id: Long) {
        // An item was selected. You can retrieve the selected item using
        Log.i("Mood:", mMoods.getItem(0).toString())
        mPickedMood = parent.getItemAtPosition(pos).toString()
        when(parent.getItemAtPosition(pos).toString()){
            mMoods.getItem(0) -> binding.imgMood.setImageResource(R.drawable.grinning_face)
            mMoods.getItem(1) -> binding.imgMood.setImageResource(R.drawable.loudly_crying_face)
            mMoods.getItem(2) -> binding.imgMood.setImageResource(R.drawable.pouting_face)
            mMoods.getItem(3) -> binding.imgMood.setImageResource(R.drawable.sleeping_face)
        }
    }

    private fun chooseGame(){
        // gets data to make prediction
        val automaticity = binding.spinnerAutomaticity.selectedItem.toString().toInt()
        val data = GamePickerData(
            mood = mPickedMood,
            motivation = mMotivationListAdapter.motivationScore,
            age = age,
            automaticity = automaticity,
            currentStreak = currentStreak,
            difficultyLevel = mHabit.difficulty,
            longestStreak = longestStreak,
            duration = mHabit.duration,
            type = mHabit.type,
            time = System.currentTimeMillis() % (1000*60*60*24),
            motivationAfter = -1)

        val choice = GamePicker.pickGame(activity, data)
        binding.txtNoHabitWarning.apply {
            visibility = View.VISIBLE
            text = choice
            setGame(choice)
        }
    }

    private fun setMood(mood: String){
        val moods = resources.getStringArray(R.array.moods_array)
        val position = moods.indexOf(mood)
        binding.spinnerMood.setSelection(position)
    }

    private fun setGame(game: String){
        val games = resources.getStringArray(R.array.game_choices)
        val position = games.indexOf(game)
        binding.spinnerGamePick.setSelection(position)
    }

    private fun launchMoodDetector(){
        val intent = Intent(activity, MoodDetectorActivity::class.java)
        startActivityForResult(intent, 5)
    }

    private fun addUserDay(automaticity: Int, motivationScore: Int, mood: String){
        val userDay = Helper.getCurrentUserDayNo()

        val data: Map<String, Any> = mapOf(
            "day_number" to userDay,
            "motivation" to motivationScore,
            "mood" to mood,
            "automaticity" to automaticity
        )

        val database = Firebase.database
        val currentUser = FirebaseAuth.getInstance().currentUser!!.uid
        val myDatabase = database.getReference("days/$currentUser")

        val myDay = myDatabase
            .orderByChild("day_number")
            .equalTo(userDay.toDouble())
            .limitToFirst(1)

        myDay.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) { }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var key = myDatabase.push().key
                if (dataSnapshot.exists()) {
                    for (child in dataSnapshot.children) {
                        key = child.key
                    }
                    Log.e("value", dataSnapshot.value.toString())
                }

                if (key == null) {
                    Log.e("sendDay", "Couldn't get push key for Days")
                }else{
                    val childUpdates = hashMapOf<String, Any>(
                        "/$key" to data
                    )

                    myDatabase.updateChildren(childUpdates)
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 5) {
            (if (data != null) data.getStringExtra("DetectedMood") else "")
                .also { setMood(it) }
        }
    }
}