package com.example.levelup.ui.gallery

import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class GalleryViewModel : ViewModel() {

    private lateinit var mTwitterIdKey: String
    private lateinit var mTicketKey: String
    private val preferenceChangeListener =
        OnSharedPreferenceChangeListener { sharedPreferences, key ->
            if(key == mTwitterIdKey){
                val twitterId = sharedPreferences.getString(key, "")
                _mood_button_enabled.apply {
                    value = (twitterId!="")
                }
            }else if(key == mTicketKey){
                val ticketNo = sharedPreferences.getString(key, "")
                _ticket.apply {
                    value = ticketNo.toString()
                }
            }
        }

    private val _text = MutableLiveData<String>().apply {
        value = ""
    }

    private val _mood_text = MutableLiveData<String>().apply {
        value = "What mood are you currently feeling?"
    }

    private val _motivation_text = MutableLiveData<String>().apply {
        value = "How motivated are you currently feeling?"
    }

    private val _mood_button_enabled = MutableLiveData<Boolean>().apply {
        value = true
    }

    private val _ticket = MutableLiveData<String>().apply {
        value = "0"
    }

    fun setupPreferenceListener(sharedPrefs: SharedPreferences, twitterIdKey: String, ticketKey: String){
        val ticketNo = sharedPrefs.getString(ticketKey, "0")
        val twitterId = sharedPrefs.getString(twitterIdKey, "")
        _mood_button_enabled.apply {
            value = twitterId!=""
        }
        _ticket.apply {
            value = ticketNo
        }
        mTwitterIdKey = twitterIdKey
        mTicketKey = ticketKey
        sharedPrefs.registerOnSharedPreferenceChangeListener(preferenceChangeListener)
    }

    val text: LiveData<String> = _text
    val moodText: LiveData<String> = _mood_text
    val motivationText: LiveData<String> = _motivation_text
    val ticketText: LiveData<String> = _ticket
    val moodButtonEnabled: LiveData<Boolean> = _mood_button_enabled
}