package com.example.levelup.ui.help

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.levelup.R
import com.example.levelup.databinding.FragmentHelpBinding
import com.example.levelup.databinding.FragmentSlideshowBinding

class HelpFragment : Fragment() {
    private var _binding: FragmentHelpBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHelpBinding.inflate(inflater,
            container,
            false)
        setupHyperlink()
        return binding.root
    }


    fun setupHyperlink() {
        val linkTextView = binding.textView3
        linkTextView.movementMethod = LinkMovementMethod.getInstance();
    }
}