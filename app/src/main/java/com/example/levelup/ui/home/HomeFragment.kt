package com.example.levelup.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.levelup.*
import com.example.levelup.adapter.HabitListAdapter
import com.example.levelup.databinding.FragmentHomeBinding
import com.example.levelup.habitManager.Habit
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import java.util.*

class HomeFragment : Fragment() {
    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private val habitViewModel: HabitViewModel by viewModels {
        HabitViewModelFactory((activity?.application as HabitsApplication).repository)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel = ViewModelProviders
            .of(this)
            .get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding
            .inflate(inflater,
                container,
                false)

        homeViewModel.text.observe(viewLifecycleOwner, Observer {
            binding.textHome.text = it
        })

        binding.fab.setOnClickListener {
            val intent = Intent(activity, CreateHabitActivity::class.java)
            startActivity(intent)
        }
        // Sets adapter for Habits List

        val adapter = HabitListAdapter()
        adapter.onItemClick = { habit ->
            onHabitClick(habit)
        }
        adapter.onButtonClick = { id ->
            onHabitButtonClick(id)
        }
        binding.recyclerviewHabitsList.adapter = adapter
        binding.recyclerviewHabitsList.layoutManager = LinearLayoutManager(context)

        // Add an observer on the LiveData returned by getAlphabetizedWords.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        habitViewModel.allHabits.observe(viewLifecycleOwner, Observer {habits->
            // Update the cached copy of the words in the adapter.
            habits.let { adapter.submitList(it) }
        })

        return binding.root
    }

    private fun onHabitClick(habit: Habit) {
        val intent = Intent(activity, HabitSummaryActivity::class.java)
        intent.putExtra("HabitId", habit.habitId)
        startActivity(intent)
    }

    private fun onHabitButtonClick(id: Int){
        Toast.makeText(activity, id.toString(), Toast.LENGTH_SHORT).show()
    }

    private fun readDatabase(){
        val database = Firebase.database
        val currentUser = FirebaseAuth.getInstance().currentUser!!.uid

        val myDatabase = database.getReference("days/$currentUser")
        val menuListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if(!dataSnapshot.exists()){
                    Log.i("Reading database", "no data")
                    val data = mapOf(
                        "motivation" to 5,
                        "mood" to "Happy",
                        "automaticity" to 5
                    )

                    val childUpdates = hashMapOf<String, Any>(
                        "/1" to data
                    )

                    myDatabase.updateChildren(childUpdates)
                }else{
                    Log.i("Reading database",dataSnapshot.value.toString())
                    val data = mapOf(
                        "motivation" to 3,
                        "mood" to "Happy",
                        "automaticity" to 5
                    )

                    val childUpdates = hashMapOf<String, Any>(
                        "/1" to data
                    )

                    myDatabase.updateChildren(childUpdates)
                }

            }
            override fun onCancelled(databaseError: DatabaseError) { }
        }

        myDatabase.addListenerForSingleValueEvent(menuListener)
    }
}