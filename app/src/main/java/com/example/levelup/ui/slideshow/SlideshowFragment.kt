package com.example.levelup.ui.slideshow

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chaquo.python.Python
import com.example.levelup.helper.Helper
import com.example.levelup.R
import com.example.levelup.SearchTwitterAccountActivity
import com.example.levelup.TwitterScraper.Tweet
import com.example.levelup.TwitterScraper.TwitterScraper
import com.example.levelup.adapter.PlayerScoreListAdapter
import com.example.levelup.databinding.FragmentSlideshowBinding
import com.example.levelup.game.PlayerScore
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.functions.FirebaseFunctionsException
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SlideshowFragment : Fragment() {
    private lateinit var mSlideshowViewModel: SlideshowViewModel
    private lateinit var mLayoutManager: RecyclerView.LayoutManager
    private var _binding: FragmentSlideshowBinding? = null
    private val binding get() = _binding!!
    private val TAG = "SlideShowFragment"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mSlideshowViewModel =
            ViewModelProviders.of(this).get(SlideshowViewModel::class.java)

        _binding = FragmentSlideshowBinding.inflate(inflater,
            container,
            false)

        var score = Helper.readUserSettings(requireContext(), getString(R.string.score), "0")
        binding.txtMyScore.text = score

        mLayoutManager = LinearLayoutManager(requireContext())

        getScores()

        return binding.root
    }

    private fun getScores(){
        binding.progBarGetScores.visibility = View.VISIBLE
        val database = Firebase.database
        val currentUser = FirebaseAuth.getInstance().currentUser!!.uid
        val myDatabase = database.reference
            .child("scores")
            .orderByChild("score")

        myDatabase.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val scores = readList(dataSnapshot)
                    binding.rvPlayerScores.layoutManager = mLayoutManager
                    binding.progBarGetScores.visibility = View.GONE
                    binding.rvPlayerScores.adapter = PlayerScoreListAdapter(scores, currentUser, requireContext())
                }
            }
        })
    }

    private fun readList(snapshot: DataSnapshot): MutableList<PlayerScore> {
        var scores = mutableListOf<PlayerScore>()
        for (scoreSnapshot in snapshot.children) {
            val scoreMap = scoreSnapshot.value as Map<String, Any>
            val score = PlayerScore(
                scoreMap["uid"] as String,
                scoreMap["username"] as String,
                scoreMap["score"] as Long)
                scores.add(score)
        }

        return scores.asReversed()
    }
}