import nltk
import numpy as np
import os
import pickle
import pandas as pd
import re
from pathlib import Path
from emoji import demojize
from tensorflow.keras.layers import Input, Embedding, SpatialDropout1D, LSTM
from tensorflow.keras.layers import GlobalAveragePooling1D, GlobalMaxPooling1D
from tensorflow.keras.layers import Bidirectional, Conv1D, Dense, concatenate
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Model
from time import time

nltk.download('stopwords')

def reconstructTokenizer():
    fn = os.path.join(os.path.dirname(__file__), 'tokenizer.pickle')
    tokenizer_path = Path(fn).resolve()
    with tokenizer_path.open('rb') as file:
        tokenizer = pickle.load(file)
    return tokenizer

def reconstructEncoder():
    fn = os.path.join(os.path.dirname(__file__), 'encoder.pickle')
    encoder_path = Path(fn).resolve()
    with encoder_path.open('rb') as file:
        encoder = pickle.load(file)
    return encoder

def reconstructModel():
    input_dim = min(tokenizer.num_words, len(tokenizer.word_index) + 1)
    num_classes = 4
    embedding_dim = 500
    input_length = 100
    lstm_units = 25
    lstm_dropout = 0.1
    recurrent_dropout = 0.1
    spatial_dropout=0.2
    filters=64
    kernel_size=3

    input_layer = Input(shape=(input_length,))
    output_layer = Embedding(
    input_dim=input_dim,
    output_dim=embedding_dim,
    input_shape=(input_length,)
    )(input_layer)

    output_layer = SpatialDropout1D(spatial_dropout)(output_layer)

    output_layer = Bidirectional(
    LSTM(lstm_units, return_sequences=True,
        dropout=lstm_dropout, recurrent_dropout=recurrent_dropout)
    )(output_layer)
    output_layer = Conv1D(filters, kernel_size=kernel_size, padding='valid',
                        kernel_initializer='glorot_uniform')(output_layer)

    avg_pool = GlobalAveragePooling1D()(output_layer)
    max_pool = GlobalMaxPooling1D()(output_layer)
    output_layer = concatenate([avg_pool, max_pool])

    output_layer = Dense(num_classes, activation='softmax')(output_layer)

    model = Model(input_layer, output_layer)

    fn = os.path.join(os.path.dirname(__file__), 'emotion_recognition_model_weights.h5')
    model_weights_path = Path(fn).resolve()
    model.load_weights(model_weights_path.as_posix())
    return model

def preprocess(texts, quiet=False):
    start = time()
    # Lowercasing
    texts = texts.str.lower()

    # Remove special chars
    texts = texts.str.replace(r"(http|@)\S+", "")
    texts = texts.apply(demojize)
    texts = texts.str.replace(r"::", ": :")
    texts = texts.str.replace(r"’", "'")
    texts = texts.str.replace(r"[^a-z\':_]", " ")

    # Remove repetitions
    pattern = re.compile(r"(.)\1{2,}", re.DOTALL)
    texts = texts.str.replace(pattern, r"\1")

    # Transform short negation form
    texts = texts.str.replace(r"(can't|cannot)", 'can not')
    texts = texts.str.replace(r"n't", ' not')

    # Remove stop words
    stopwords = nltk.corpus.stopwords.words('english')
    stopwords.remove('not')
    stopwords.remove('nor')
    stopwords.remove('no')
    texts = texts.apply(
        lambda x: ' '.join([word for word in x.split() if word not in stopwords])
    )

    if not quiet:
        print("Time to clean up: {:.2f} sec".format(time() - start))

    return texts

tokenizer = reconstructTokenizer()
encoder = reconstructEncoder()
model = reconstructModel()

def detectEmotion(text):
    d = {'text':text}
    ser = pd.Series(data=d)
    text = preprocess(ser, quiet = True)
    list_tokenized = tokenizer.texts_to_sequences(text)
    data_input = pad_sequences(list_tokenized, maxlen=100)

    prediction = model.predict(data_input)
    prediction = prediction.argmax(axis=1)[0]

    mood = 'N/A'
    if(prediction == 0):
        mood = 'Angry'
    elif(prediction == 1):
        mood = 'Happy'
    elif(prediction == 2):
        mood = 'Sad'
    elif(prediction == 3):
        mood = 'Tired'
    return mood

