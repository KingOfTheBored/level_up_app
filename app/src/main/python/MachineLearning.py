import os
import pickle
import pandas as pd
import numpy as np
from joblib import dump, load

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn import metrics
from sklearn.neighbors import NearestNeighbors
from sklearn.linear_model import LinearRegression
from sklearn import tree

sc_X = StandardScaler()

def pickGame(games, age, auto, current, difficulty, duration, longest, mood, motivation, time, type):
    numberOfGames = 3
    ages = [age] *numberOfGames
    autos = [auto] *numberOfGames
    currents = [current] *numberOfGames
    difficulties = [difficulty] *numberOfGames
    durations = [duration] *numberOfGames
    longests = [longest] *numberOfGames
    moods = [mood] *numberOfGames
    motivations = [motivation] *numberOfGames
    times = [time] *numberOfGames
    types = [type] *numberOfGames
    data = {'age':  ages,
     'automaticity': autos,
     'currentStreak': currents,
     'difficultyLevel': difficulties,
     'duration': durations,
     'gameChoice': ['Great_Geometry_Game','Block_Barricade_Buster','Rainbow_Rotation_Retaliation'],
     'longestStreak': longests,
     'mood': moods,
     'motivation': motivations,
     'time': times,
     'type': types,
     }

    classifier = reloadModel()
    dataPoints = pd.DataFrame (data, columns = ['age',
    'automaticity',
    'currentStreak',
    'difficultyLevel',
    'duration',
    'gameChoice',
    'longestStreak',
    'mood',
    'motivation',
    'time',
    'type'])

    dataPoints = preprocess(dataPoints)

    print(dataPoints)

    predictions = classifier.predict(dataPoints)
    predictions = predictions.tolist()
    print(predictions)

    choices = ['Great_Geometry_Game','Block_Barricade_Buster','Rainbow_Rotation_Retaliation']
    max_value = max(predictions)
    max_index = predictions.index(max_value)
    gamePick = choices[max_index]

    return gamePick

def preprocess(data):
    # One hot encoding for the mood category
    cats=['Happy','Sad','Angry','Tired']
    data.mood =pd.Categorical(data['mood'], categories=cats, ordered=True)
    moods = pd.get_dummies(data.mood, prefix='mood')
    df = pd.concat([data, moods], axis = 'columns')
    df = df.drop(['mood', 'mood_Angry'], axis = 'columns')
    cats=['Great_Geometry_Game','Block_Barricade_Buster','Rainbow_Rotation_Retaliation']
    df.gameChoice =pd.Categorical(data['gameChoice'], categories=cats, ordered=True)
    gameChoices = pd.get_dummies(df.gameChoice)

    df = pd.concat([df, gameChoices], axis = 'columns')
    df = df.drop(['gameChoice', 'Rainbow_Rotation_Retaliation'], axis = 'columns')

    # replaces ordinal values with integers
    PClassDict =   { 'Negative':0, 'Positive':1 }

    df['positive'] = df.type.map(PClassDict)
    df = df.drop(['type'], axis = 'columns')
    PDurationDict =   { 'Five mins or less':5, 'Fifteen mins or less':15, 'Hour or less':60, 'Over an hour': 90  }
    df['habit_duration'] = df.duration.map(PDurationDict)
    df = df.drop(['duration'], axis = 'columns')

    PDifficultyDict =   { 'Trivial':0, 'Easy':1, 'Medium':2, 'Hard': 3 }
    df['habit_difficulty'] = df.difficultyLevel.map(PDifficultyDict)
    df = df.drop(['difficultyLevel'], axis = 'columns')
    df = sc_X.transform(df)
    return df

def createKnnModel():
    fn = os.path.join(os.path.dirname(__file__), 'habit_data.csv')
    df = pd.read_csv(fn)

    # One hot encoding for the mood category
    moods = pd.get_dummies(df.mood, prefix='mood')
    df = pd.concat([df, moods], axis = 'columns')
    df = df.drop(['mood', 'mood_Angry'], axis = 'columns')

    gameChoices = pd.get_dummies(df.gameChoice)
    df = pd.concat([df, gameChoices], axis = 'columns')
    df = df.drop(['gameChoice', 'Rainbow_Rotation_Retaliation'], axis = 'columns')
    # replaces ordinal values with integers
    PClassDict =   { 'NEGATIVE':0, 'POSITIVE':1 }

    df['positive'] = df.type.map(PClassDict)
    df = df.drop(['type'], axis = 'columns')

    PDurationDict =   { 'FIVE_MIN':5, 'FIFTEEN_MIN':15, 'HOUR':60, 'OVER_HOUR': 90  }
    df['habit_duration'] = df.duration.map(PDurationDict)
    df = df.drop(['duration'], axis = 'columns')

    PDifficultyDict =   { 'TRIVIAL':0, 'EASY':1, 'MEDIUM':2, 'HARD': 3 }
    df['habit_difficulty'] = df.difficultyLevel.map(PDifficultyDict)
    df = df.drop(['difficultyLevel'], axis = 'columns')
    # Split data set for testing
    X = df.iloc[:, :5]
    X1 = df.iloc[:, 6:]
    X = pd.concat([X, X1], axis = 'columns')
    y = df.iloc[:, 5]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

#    path = os.path.join(os.path.dirname(__file__), 'knn_x_train1.csv')
#    pd.DataFrame(X_train).to_csv(path, index=None, header=None)

    X_train = sc_X.fit_transform(X_train)
    X_test = sc_X.transform(X_test)
    classifier = KNeighborsClassifier(n_neighbors=5, p=2, metric='euclidean')
    classifier.fit(X_train, y_train)
    y_pred = classifier.predict(X_test)
    print(metrics.mean_absolute_error(y_test, y_pred))
    print(metrics.mean_squared_error(y_test, y_pred))
    fn = os.path.join(os.path.dirname(__file__), 'wdwgfh.joblib')
    print (fn)
    dump(classifier, fn)

    fn = os.path.join(os.path.dirname(__file__), 'Scaler.joblib')
    dump(sc_X, fn)

    path = os.path.join(os.path.dirname(__file__), 'knn_y_train1.csv')
    pd.Series(y_train).to_csv(path, header=None)

    return classifier

def getScaler():
    fn = os.path.join(os.path.dirname(__file__), 'habit_data.csv')
    df = pd.read_csv(fn)

    # One hot encoding for the mood category
    moods = pd.get_dummies(df.mood, prefix='mood')
    df = pd.concat([df, moods], axis = 'columns')
    df = df.drop(['mood', 'mood_Angry'], axis = 'columns')

    gameChoices = pd.get_dummies(df.gameChoice)
    df = pd.concat([df, gameChoices], axis = 'columns')
    df = df.drop(['gameChoice', 'Rainbow_Rotation_Retaliation'], axis = 'columns')
    # replaces ordinal values with integers
    PClassDict =   { 'NEGATIVE':0, 'POSITIVE':1 }

    df['positive'] = df.type.map(PClassDict)
    df = df.drop(['type'], axis = 'columns')

    PDurationDict =   { 'FIVE_MIN':5, 'FIFTEEN_MIN':15, 'HOUR':60, 'OVER_HOUR': 90  }
    df['habit_duration'] = df.duration.map(PDurationDict)
    df = df.drop(['duration'], axis = 'columns')

    PDifficultyDict =   { 'TRIVIAL':0, 'EASY':1, 'MEDIUM':2, 'HARD': 3 }
    df['habit_difficulty'] = df.difficultyLevel.map(PDifficultyDict)
    df = df.drop(['difficultyLevel'], axis = 'columns')
    # Split data set for testing
    X = df.iloc[:, :5]
    X1 = df.iloc[:, 6:]
    X = pd.concat([X, X1], axis = 'columns')
    sc_X.fit_transform(X)

def reloadModel():
     #return reloadReg()
     return reloadKnn()
     #return reloadDt()

def reloadReg():
    path = os.path.join(os.path.dirname(__file__), "coef.pickle")
    file = open(path, 'rb')
    coef = pickle.load(file)
    file.close()

    path = os.path.join(os.path.dirname(__file__), "intercept.pickle")
    file = open(path, 'rb')
    intercept = pickle.load(file)
    file.close()

    path = os.path.join(os.path.dirname(__file__), "reg_x_train.csv")
    X = pd.read_csv(path, header = None, index_col = 0, squeeze = True)
    sc_X.fit_transform(X)
    reg = LinearRegression()
    reg.coef_ = coef
    reg.intercept_ = intercept
    return reg

def reloadKnn():
    path = os.path.join(os.path.dirname(__file__), "knn_y_train.csv")
    y_train = pd.read_csv(path, header = None, index_col = 0, squeeze = True)

    path = os.path.join(os.path.dirname(__file__), 'knn_x_train.csv')
    arraydf= pd.read_csv(path, header = None, index_col = 0, squeeze = True)
    X_train = sc_X.fit_transform(arraydf)

    knn = KNeighborsClassifier(n_neighbors=3, p=2, metric='euclidean')
    knn.fit(X_train, y_train)
    return knn

def reloadDt():
    path = os.path.join(os.path.dirname(__file__), "dt_y_train.csv")
    y_train = pd.read_csv(path, header = None, index_col = 0, squeeze = True)

    path = os.path.join(os.path.dirname(__file__), 'dt_x_train.csv')
    arraydf= pd.read_csv(path, header = None, index_col = 0, squeeze = True)
    X_train = sc_X.fit_transform(arraydf)

    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(X_train, y_train)
    return clf