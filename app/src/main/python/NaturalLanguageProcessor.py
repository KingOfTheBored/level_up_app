import nltk
import string
import re
import os
import pickle

from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.tokenize import TweetTokenizer
from nltk import NaiveBayesClassifier
from nltk import classify

fn = os.path.join(os.path.dirname(__file__), 'my_classifier.pickle')
f = open(fn, 'rb')
classifier = pickle.load(f)
f.close()

nltk.download('stopwords')
stopwords_english = stopwords.words('english')
stemmer = PorterStemmer()

# Happy Emoticons
emoticons_happy = set([
    ':-)', ':)', ';)', ':o)', ':]', ':3', ':c)', ':>', '=]', '8)', '=)', ':}',
    ':^)', ':-D', ':D', '8-D', '8D', 'x-D', 'xD', 'X-D', 'XD', '=-D', '=D',
    '=-3', '=3', ':-))', ":'-)", ":')", ':*', ':^*', '>:P', ':-P', ':P', 'X-P',
    'x-p', 'xp', 'XP', ':-p', ':p', '=p', ':-b', ':b', '>:)', '>;)', '>:-)',
    '<3'
])

# Sad Emoticons
emoticons_sad = set([
    ':L', ':-/', '>:/', ':S', '>:[', ':@', ':-(', ':[', ':-||', '=L', ':<',
    ':-[', ':-<', '=\\', '=/', '>:(', ':(', '>.<', ":'-(", ":'(", ':\\', ':-c',
    ':c', ':{', '>:\\', ';('
])

# all emoticons (happy + sad)
emoticons = emoticons_happy.union(emoticons_sad)

# removes stock market labels, retweet refrences, hyperlinks, and hashtags
def processTweet(tweet):
    tweet = re.sub(r'\$\w*', '', tweet)
    tweet = re.sub(r'^RT[\s]+', '', tweet)
    tweet = re.sub(r'https?:\/\/.*[\r\n]*', '', tweet)
    tweet = re.sub(r'#', '', tweet)

    # tokenize tweets
    # lowerscase, removes twitter handles, removes extra letters in words
    tokenizer = TweetTokenizer(preserve_case=False, strip_handles=True, reduce_len=True)
    tweet_tokens = tokenizer.tokenize(tweet)

    tweets_clean = []
    for word in tweet_tokens:
        if (word not in stopwords_english and # remove stopwords
                word not in emoticons and # remove emoticons
                word not in string.punctuation): # remove punctuation
            #tweets_clean.append(word)
            stem_word = stemmer.stem(word) # stemming word
            tweets_clean.append(stem_word)

    return tweets_clean

def bag_of_words(tweet):
    words = processTweet(tweet)
    words_dictionary = dict([word, True] for word in words)
    return words_dictionary

def classify_tweet(tweet):
    custom_tweet_set = bag_of_words(tweet)
    return classifier.classify(custom_tweet_set)

def classify_results(tweet):
    custom_tweet_set = bag_of_words(tweet)
    prob_result = classifier.prob_classify(custom_tweet_set)
    sentiment = prob_result.max()
    sentiment_text = ""
    probability = prob_result.prob(sentiment)
    if (sentiment == "pos"):
        sentiment_text = "positive"
    else:
        sentiment_text = "negative"
    return "This tweet was classed as "+sentiment_text+" with a probability of "+"{:.2f}".format(probability)