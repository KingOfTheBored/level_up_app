import nltk
from nltk.corpus import treebank
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer

def plot():
    nltk.download('punkt')
    nltk.download('stopwords')
    stop_words=set(stopwords.words("english"))
    sentence = """At eight o'clock on Thursday morning... Arthur didn't feel very good."""
    filtered_sent=[]
    tokens = nltk.word_tokenize(sentence)
    for w in tokens:
        if w not in stop_words:
            filtered_sent.append(w)

    # Stemming
    ps = PorterStemmer()
    stemmed_words=[]
    for w in filtered_sent:
        stemmed_words.append(ps.stem(w))
    return stemmed_words