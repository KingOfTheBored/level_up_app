package com.example.levelup

import com.example.levelup.habitManager.Day
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun longestStreak() {
        val days = listOf(Day(0,0,0),
            Day(0,1,1),
            Day(0,2,1),
            Day(0,3,1),
            Day(0,4,0),
            Day(0,5,1),
            Day(0,6,1),
        )
        val currentStreak = Day.currentStreak(days,6)
        assertEquals(2, currentStreak)

        val longestStreak = Day.longestStreak(days)
        assertEquals(3, longestStreak)
    }
}