const Twit = require("twit") 
const apiKey = "5XulH5PXhe0yO0imrta0sGZeH";
const apiSecretKey = "CNmFnbTEQH0ounnTAwuUFswyCeJZ9a2cywMcoZ2wZNIAJvPoeR";
const accessToken = "2830395516-F7U5lTbSomzS7gAGk9OgxbFFEdp1LhxAeZcBwzK";
const accessTokenSecret = "vhQJ6xSZwn2iiS3xQVgAPUf18xRQlwSBPl52W5sqDKuF5";

var T = new Twit({
  consumer_key:         apiKey,
  consumer_secret:      apiSecretKey,
  access_token:         accessToken,
  access_token_secret:  accessTokenSecret,
});

// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access Cloud Firestore.
const admin = require('firebase-admin');
admin.initializeApp()

// Firebase function to scrape tweets from user
exports.scrapeTwitter = functions.https.onCall((data, context) => {
	// Message text passed from the client.
  const twitterId = data.twitter_id;
  
	// Authentication / user information is automatically added to the request.
	const uid = context.auth.uid;
	const name = context.auth.token.name || null;
	const picture = context.auth.token.picture || null;
	const email = context.auth.token.email || null;

	var results = {
    	tweetTexts: []
	};

	//1. GET RECENT TWEETS
  return getUserTweets(twitterId).
  catch(function (err) {
    console.log('caught error', err.stack)
  })
  .then(function (result) {
    const tweets = result.data
		.map(tweet => results.tweetTexts.push({
      "text": tweet.full_text,
      "id": tweet.id_str,
      "created_at": tweet.created_at
    }));
 
    functions.logger.info('data', result.data);
    return results;
  })
});

// Firebase function to search twitter users from username
exports.searchTwitterUsers = functions.https.onCall((data, context) => {
	// Message text passed from the client.
  const username = data.username;
  var results = {
    userAccounts: []
  };

  return getUsers(username)
    .catch(function (err) {
      console.log('caught error', err.stack)
    })
    .then(function (result) {
      result.data
		    .map(user => results.userAccounts.push({
          "id": user.id_str,
          "name": user.name,
          "screenName": user.screen_name,
          "profileImageUrl": user.profile_image_url_https,
          "verified": user.verified
        }));        
        
      functions.logger.info('data', results);
      return results;
    })

});

const getUserTweets = async (userId) => {
  return T.get('statuses/user_timeline', { 
    user_id: `${userId}`,
    count: 1 ,
    tweet_mode: "extended"
  })
}

const getUsers = async (username) => {
  return T.get('users/search', { 
    q: `${username}`,
    count: 10 
  })
}